package cz.fel.barysole.pdaeshop.ui.shops

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class ShopsViewModel : ViewModel() {

    val chosenSourceNameLiveData = MutableLiveData<String>()

    fun onSourceChosen(chosenDeviceName: String) {
        chosenSourceNameLiveData.value = chosenDeviceName
    }

}