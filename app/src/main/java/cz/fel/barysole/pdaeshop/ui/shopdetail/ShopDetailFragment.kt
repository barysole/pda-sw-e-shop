package cz.fel.barysole.pdaeshop.ui.shopdetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentShopDetailBinding
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.base.BaseItemDecoration
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.shopdetail.adapter.ShopServiceAdapter
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShopDetailFragment : BaseFragment() {

    private var _binding: FragmentShopDetailBinding? = null

    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var scrollView: NestedScrollView
    private lateinit var shopDetailImage: ImageView
    private lateinit var shopDetailDescription: TextView
    private lateinit var shopDetailOpeningHoursValue: TextView
    private lateinit var shopDetailDescriptionLarge: TextView


    @Inject
    lateinit var adapter: ShopServiceAdapter

    private val shopDetailViewModel by viewModels<ShopDetailViewModel>()
    private val args: ShopDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShopDetailBinding.inflate(inflater, container, false)
        val root: View = binding.root

        loaderHolder = binding.loaderHolder
        loaderIcon = binding.loaderIcon
        noDataButton = binding.noDataButton
        noDataTextView = binding.noDataText
        scrollView = binding.detailScrollView

        shopDetailImage = binding.shopDetailImage
        shopDetailDescription = binding.shopDetailDescription
        shopDetailOpeningHoursValue = binding.shopDetailOpeningHoursValue
        shopDetailDescriptionLarge = binding.shopDetailDescriptionLarge

        recyclerView = binding.serviceRecycler
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            BaseItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.service_icon_margin)
            )
        )

        shopDetailViewModel.apply {
            setCurrentShopId(args.shopId)
            shopLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { shop -> drawShopDetailData(shop) })
            isShopLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            noShopDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { error -> showError(error) })
            start()
            loadShop(args.shopId)
        }

        return root
    }

    @SuppressLint("SetTextI18n")
    private fun drawShopDetailData(shopModel: ShopModel) {
        ImageLoader(
            shopDetailImage,
            R.drawable.e_shop_logo_icon_with_padding,
            shopModel.imageUrl,
            isCenterCrop = true
        ).load()
        shopDetailDescription.text = shopModel.address
        shopDetailOpeningHoursValue.text =
            shopModel.openingTime?.dropLast(3) + " - " + shopModel.closingTime?.dropLast(3)
        shopDetailDescriptionLarge.text = shopModel.description
        shopModel.serviceList?.apply {
            adapter.setItems(ArrayList(this))
        }
        (activity as AppCompatActivity).supportActionBar?.subtitle = shopModel.name
    }

    override fun setNoData(noData: Boolean) {
        super.setNoData(noData)
        if (noData) {
            scrollView.visibility = View.GONE
        } else {
            scrollView.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
        super.onDestroy()
    }

}