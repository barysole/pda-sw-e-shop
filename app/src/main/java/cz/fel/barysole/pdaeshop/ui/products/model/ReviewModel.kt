package cz.fel.barysole.pdaeshop.ui.products.model

import java.math.BigDecimal

class ReviewModel {
    var id: String = ""

    var author: String = ""

    var advantages: String = ""

    var disadvantages: String = ""

    var rating: BigDecimal = BigDecimal.ZERO

    var creationTimestamp: String = ""

    var productId: String = ""
}