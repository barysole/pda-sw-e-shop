package cz.fel.barysole.pdaeshop.ui.products.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GridItemDecoration(private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val spanCount = (parent.layoutManager as GridLayoutManager).spanCount
        val xPos = position % spanCount
        val yPos = position / spanCount

        val isFirstColumn = xPos == 0
        val isFirstRow = yPos == 0
        val isLastColumn = xPos == spanCount - 1

        super.getItemOffsets(outRect, view, parent, state)
        if (!isFirstRow) {
            outRect.top = spacing
        }
        if (!isFirstColumn) {
            outRect.left = spacing
        }

    }

}