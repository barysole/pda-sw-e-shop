package cz.fel.barysole.pdaeshop.ui.productfilter.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import java.util.Locale
import javax.inject.Inject

class ProductsTypeAdapter @Inject constructor() :
    BaseRecyclerAdapter<ProductsTypeAdapter.ProductTypeViewHolder>() {

    private var items = arrayListOf<ProductTypeModel>()
    var onProductTypeItemClickListener: ((product: ProductTypeModel, position: Int) -> Unit)? = null


    /* ViewHolder for displaying header. */

    inner class ProductTypeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val productTypeIcon: ImageView = itemView.findViewById(R.id.filterIcon)
        private val productTypeText: TextView = itemView.findViewById(R.id.filterName)
        private val productTypeCheckBox: ImageView = itemView.findViewById(R.id.filterCheckbox)

        init {
            view.setOnClickListener {
                onProductTypeItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(productType: ProductTypeModel) {
            productTypeText.text = productType.name.substring(0, 1)
                .uppercase(Locale.ROOT) + productType.name.substring(1)
                .lowercase(Locale.getDefault());
            //todo: move to utils
            when (productType.id) {
                "b6253199-15e8-48f6-8e70-b2a6ea68fc70" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_liquor_24
                        )
                    )
                }

                "175275c4-337d-49cf-93c7-276e008ee665" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_crop_16_9_24
                        )
                    )
                }

                "5a92eb53-04a6-4045-9dbc-fdefe5e73989" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_emoji_food_beverage_24
                        )
                    )
                }

                "8ed486e4-f611-4bf9-8c24-c3e08d365477" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_cake_24
                        )
                    )
                }

                "b89de19f-d13a-4ef3-897a-515798bf58a2" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_phone_android_24
                        )
                    )
                }

                "f6b1216d-4890-4b39-aaa4-affe9d324a46" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_tablet_android_24
                        )
                    )
                }

                "042cce46-ba16-42a3-b4e2-29b05ec555e4" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_laptop_24
                        )
                    )
                }

                "9f526e8e-c155-4ce7-82d4-c2706dcdc127" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_headphones_24
                        )
                    )
                }

                "79539e38-3111-40a8-8ff9-05dbfebe4233" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_computer_24
                        )
                    )
                }

                "e593a84f-e016-440a-8ab8-c255e5d03b37" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_kitchen_24
                        )
                    )
                }

                "2e69ebd4-b262-4db1-9be0-d1db29f24b33" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_soup_kitchen_24
                        )
                    )
                }

                "9ad678be-7708-4c29-aa6d-1747219acb4d" -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_business_center_24
                        )
                    )
                }

                else -> {
                    productTypeIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_filter_alt_24
                        )
                    )
                }
            }
            if (!productType.isInFilter) {
                productTypeCheckBox.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.baseline_check_box_outline_blank_24
                    )
                )
            } else {
                productTypeCheckBox.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.baseline_check_box_24
                    )
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductTypeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_filter, parent, false)
        return ProductTypeViewHolder(view)
    }


    override fun onBindViewHolder(holder: ProductTypeViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ProductTypeModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun setItem(item: ProductTypeModel, position: Int) {
        this.items[position] = item
        notifyItemChanged(position)
    }
}
