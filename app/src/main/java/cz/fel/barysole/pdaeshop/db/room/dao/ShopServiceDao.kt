package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ShopServicesEntity

@Dao
interface ShopServiceDao : BaseDao<ShopServicesEntity> {

    @Query("SELECT * FROM ShopService WHERE serviceId = :id")
    fun getById(id: String): ShopServicesEntity?

    @Query("SELECT * FROM ShopService")
    fun getAll(): List<ShopServicesEntity>

    @Query("DELETE FROM ShopService")
    fun deleteAll()

}