package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import androidx.room.util.TableInfo.*
import cz.fel.barysole.pdaeshop.db.room.converter.BigDecimalStringConverter
import java.math.BigDecimal


@Entity(tableName = "Shop")
@TypeConverters(BigDecimalStringConverter::class)
class ShopEntity {

    @PrimaryKey
    var shopId: String = ""

    var name: String? = null

    var description: String? = null

    var rating: BigDecimal = BigDecimal(0)

    var address: String? = null

    var latitude: BigDecimal? = null

    var longitude: BigDecimal? = null

    var openingTime: String? = null

    var closingTime: String? = null

    var imageUrl: String? = null

}