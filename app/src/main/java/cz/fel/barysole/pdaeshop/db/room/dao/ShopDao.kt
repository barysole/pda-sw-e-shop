package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ShopEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopWithServices

@Dao
interface ShopDao : BaseDao<ShopEntity> {

    @Transaction
    @Query("SELECT * FROM Shop")
    fun getShopListWithServices(): List<ShopWithServices>

    @Transaction
    @Query("SELECT * FROM Shop WHERE shopId = :id")
    fun getShopWithServices(id: String): ShopWithServices?

    @Transaction
    @Query("DELETE FROM Shop")
    fun deleteAll()

}