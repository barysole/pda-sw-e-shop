package cz.fel.barysole.pdaeshop.api.model.productInCart

import com.google.gson.annotations.SerializedName

class ProductInCartApiEntity {
    @SerializedName("productId")
    var productId: String = ""

    @SerializedName("amount")
    var amount: Int = 0
}