package cz.fel.barysole.pdaeshop.repository.products

import cz.fel.barysole.pdaeshop.db.room.dao.ProductTypeDao
import cz.fel.barysole.pdaeshop.repository.error.SimpleUserError
import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.repository.mapper.ProductMapper
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductTypeRepositoryImpl @Inject constructor(
    private val productTypeDao: ProductTypeDao,
    private val productMapper: ProductMapper
) : ProductTypeRepository {

    override fun loadProductsType(): Single<ProductTypeRepository.Result> {
        return Single.fromCallable { productTypeDao.getAll() }
            .map { productTypes ->
                val productTypesList = productMapper.productTypeEntityListToModelList(productTypes)
                ProductTypeRepository.Result(UserError.NO_ERROR, productTypesList)
            }
            .onErrorReturn { error ->
                ProductTypeRepository.Result(
                    SimpleUserError(
                        error.message ?: "Something went wrong..."
                    ), emptyList()
                )
            }
    }

    override fun changeProductType(productType: ProductTypeModel): Single<ProductTypeRepository.Result> {
        return Single.fromCallable {
            productTypeDao.update(productMapper.productTypeModelToEntity(productType))
            ProductTypeRepository.Result(UserError.NO_ERROR, null)
        }
            .onErrorReturn { error ->
                ProductTypeRepository.Result(
                    SimpleUserError(
                        error.message ?: "Something went wrong..."
                    ), null
                )
            }
    }

}