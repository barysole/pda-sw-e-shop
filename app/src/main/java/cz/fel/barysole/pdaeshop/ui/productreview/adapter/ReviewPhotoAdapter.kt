package cz.fel.barysole.pdaeshop.ui.productreview.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import javax.inject.Inject

class ReviewPhotoAdapter @Inject constructor() : BaseRecyclerAdapter<RecyclerView.ViewHolder>() {

    private var items = arrayListOf<ReviewPhotoItem>()
    var onAddPhotoItemClickListener: ((position: Int) -> Unit)? = null
    var onPhotoItemClickListener: ((position: Int) -> Unit)? = null

    inner class ReviewPhotoAddHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val addImage: ImageView = itemView.findViewById(R.id.add_image)

        init {
            view.setOnClickListener {
                onAddPhotoItemClickListener?.invoke(adapterPosition)
            }
        }

        fun bind(item: ReviewPhotoAddItem) {

        }

    }

    inner class ReviewPhotoImageHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val reviewImage: ImageView = itemView.findViewById(R.id.review_image)

        init {
            view.setOnClickListener {
                onPhotoItemClickListener?.invoke(adapterPosition)
            }
        }

        fun bind(item: ReviewPhotoImageItem) {
            reviewImage.setImageBitmap(item.photoBitmap)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                ReviewPhotoAddHolder(
                    layoutInflater.inflate(
                        R.layout.item_review_photo_add,
                        parent,
                        false
                    )
                )
            }

            1 -> {
                ReviewPhotoImageHolder(
                    layoutInflater.inflate(
                        R.layout.item_review_photo,
                        parent,
                        false
                    )
                )
            }

            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $viewType not supported")
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is ReviewPhotoAddItem -> {
                0
            }

            is ReviewPhotoImageItem -> {
                1
            }

            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $position not supported")
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (val item = items[position]) {
            is ReviewPhotoAddItem -> {
                (holder as ReviewPhotoAddHolder).bind(item)
            }

            is ReviewPhotoImageItem -> {
                (holder as ReviewPhotoImageHolder).bind(item)
            }

            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $position not supported")
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ReviewPhotoItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun insertItem(item: ReviewPhotoItem, position: Int) {
        this.items.add(position, item)
        notifyItemRangeChanged(position, itemCount)
    }

}