package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ShopAndServiceEntity

@Dao
interface ShopAndServiceDao : BaseDao<ShopAndServiceEntity> {
}