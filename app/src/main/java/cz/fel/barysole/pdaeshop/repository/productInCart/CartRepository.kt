package cz.fel.barysole.pdaeshop.repository.productInCart

import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import io.reactivex.rxjava3.core.Single

interface CartRepository {

    fun loadProductsInCart(): Single<Result>

    fun saveProductToCart(product: ProductModel): Single<UserError>

    class Result(val userError: UserError, val list: List<ProductInCartModel>) {
        val isSuccessful: Boolean
            get() = userError === UserError.NO_ERROR
    }

    fun updateProductInCart(productInCart: ProductInCartModel): Single<UserError>
    fun clearTheCart(): Single<UserError>
}