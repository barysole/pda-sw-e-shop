package cz.fel.barysole.pdaeshop.api

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class AppApiModule {

    @Singleton
    @Provides
    fun provideAppDb(@ApplicationContext context: Context): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ESHOP_SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    internal fun productApi(retrofit: Retrofit): EshopServerApi {
        return retrofit.create(EshopServerApi::class.java)
    }

    companion object {
        const val ESHOP_SERVER_URL = "http://104.248.143.245:8335/"
    }

}