package cz.fel.barysole.pdaeshop.repository.productInCart

import cz.fel.barysole.pdaeshop.db.room.dao.ProductDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductInCartDao
import cz.fel.barysole.pdaeshop.db.room.model.ProductInCartEntity
import cz.fel.barysole.pdaeshop.repository.error.SimpleUserError
import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.repository.mapper.ProductMapper
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CartRepositoryImpl @Inject constructor(
    private val productInCartDao: ProductInCartDao,
    private val productDao: ProductDao,
    private val productMapper: ProductMapper
) : CartRepository {

    override fun clearTheCart(): Single<UserError> {
        return Single.fromCallable {
            productInCartDao.deleteAll()
            return@fromCallable UserError.NO_ERROR
        }.onErrorReturn { error ->
            return@onErrorReturn SimpleUserError(error.message ?: error.toString())
        }
    }

    override fun loadProductsInCart(): Single<CartRepository.Result> {
        return Single.fromCallable { productInCartDao.getAll() }
            .map { productInCartList ->
                val resultList = mutableListOf<ProductInCartModel>()
                for (productInCart in productInCartList) {
                    val newProductInCart = ProductInCartModel()
                    newProductInCart.amount = productInCart.amount
                    val productEntity = productDao.getById(productInCart.productId)
                    if (productEntity != null) {
                        newProductInCart.product =
                            productMapper.entityToModel(productEntity)
                    } else {
                        productInCartDao.delete(productInCart)
                    }
                    resultList.add(newProductInCart)
                }
                return@map CartRepository.Result(
                    UserError.NO_ERROR,
                    resultList
                )
            }
            .onErrorReturn { error ->
                return@onErrorReturn CartRepository.Result(
                    SimpleUserError(
                        error.message ?: error.toString()
                    ), emptyList()
                )
            }
    }

    override fun saveProductToCart(product: ProductModel): Single<UserError> {
        return Single.fromCallable {
            val oldProductInCart = productInCartDao.getById(product.id)
            if (oldProductInCart == null) {
                val productInCartEntity = ProductInCartEntity()
                productInCartEntity.productId = product.id
                productInCartEntity.amount = 1
                productInCartDao.insert(productInCartEntity)
                return@fromCallable UserError.NO_ERROR
            } else {
                oldProductInCart.amount++;
                productInCartDao.update(oldProductInCart)
                return@fromCallable UserError.NO_ERROR
            }
        }.onErrorReturn { error ->
            return@onErrorReturn SimpleUserError(error.message ?: error.toString())
        }
    }

    override fun updateProductInCart(productInCart: ProductInCartModel): Single<UserError> {
        return Single.fromCallable {
            val oldProductInCart = productInCartDao.getById(productInCart.product.id)
            if (oldProductInCart == null) {
                if (productInCart.amount != 0) {
                    val productInCartEntity = ProductInCartEntity()
                    productInCartEntity.productId = productInCart.product.id
                    productInCartEntity.amount = productInCart.amount
                    productInCartDao.insert(productInCartEntity)
                }
                return@fromCallable UserError.NO_ERROR
            } else {
                if (productInCart.amount != 0) {
                    oldProductInCart.productId = productInCart.product.id
                    oldProductInCart.amount = productInCart.amount
                    productInCartDao.update(oldProductInCart)
                } else {
                    productInCartDao.delete(oldProductInCart)
                }
                return@fromCallable UserError.NO_ERROR
            }
        }.onErrorReturn { error ->
            return@onErrorReturn SimpleUserError(error.message ?: error.toString())
        }
    }


}