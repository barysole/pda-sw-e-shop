package cz.fel.barysole.pdaeshop.ui.orderconfirmation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import cz.fel.barysole.pdaeshop.databinding.FragmentOrderConfirmationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderConfirmationFragment : Fragment() {

    private lateinit var navController: NavController

    private var _binding: FragmentOrderConfirmationBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var continueButton: Button

    private val viewModel by viewModels<OrderConfirmationViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOrderConfirmationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        navController = findNavController()

        continueButton = binding.continueButton
        continueButton.setOnClickListener {
            navController.navigate(OrderConfirmationFragmentDirections.actionOrderConfirmationFragmentToProductsFragment())
        }

        return root
    }


}