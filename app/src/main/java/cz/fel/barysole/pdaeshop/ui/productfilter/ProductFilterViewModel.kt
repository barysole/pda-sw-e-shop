package cz.fel.barysole.pdaeshop.ui.productfilter

import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.products.ProductTypeRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ProductFilterViewModel @Inject constructor(val productTypeRepository: ProductTypeRepository) :
    BaseViewModel() {

    val productsTypeLiveData = MutableLiveData<List<ProductTypeModel>>()
    val noDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()
    val changedProductTypeLiveData = SingleEventLiveData<Pair<ProductTypeModel, Int>>()

    private var loadProductsTypesDisposable = Disposable.disposed()
    private var changeProductTypeDisposable = Disposable.disposed()

    fun loadProductsType() {
        loadProductsTypesDisposable = productTypeRepository.loadProductsType()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (!result.list.isNullOrEmpty()) {
                    productsTypeLiveData.value = result.list!!
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noDataLiveData.value = result.list?.isEmpty()
            }, {
                noDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun onFilterItemClick(productType: ProductTypeModel, position: Int) {
        productType.isInFilter = !productType.isInFilter
        changeProductTypeDisposable = productTypeRepository.changeProductType(productType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                } else {
                    changedProductTypeLiveData.value = Pair(productType, position)
                }
            }, {
                errorLiveData.value = it.message
            })
    }

    override fun onStart() {
        //nothing
    }

    override fun onCleared() {
        loadProductsTypesDisposable.dispose()
        changeProductTypeDisposable.dispose()
        super.onCleared()
    }
}