package cz.fel.barysole.pdaeshop.api.model.shop

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class ShopApiEntity {

    @SerializedName("id")
    val id: String = ""

    @SerializedName("name")
    val name: String? = null

    @SerializedName("description")
    val description: String? = null

    @SerializedName("rating")
    val rating: BigDecimal = BigDecimal(0)

    @SerializedName("address")
    val address: String? = null

    @SerializedName("latitude")
    val latitude: BigDecimal? = null

    @SerializedName("longitude")
    val longitude: BigDecimal? = null

    @SerializedName("openingTime")
    val openingTime: String? = null

    @SerializedName("closingTime")
    val closingTime: String? = null

    @SerializedName("imageUrl")
    val imageUrl: String? = null

    @SerializedName("serviceList")
    val serviceList: List<ShopServiceApiEntity>? = null

}