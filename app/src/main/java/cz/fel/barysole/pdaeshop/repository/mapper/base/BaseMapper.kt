package cz.fel.barysole.pdaeshop.repository.mapper.base

interface BaseMapper<M, A, E> {
    fun apiEntityToModel(entity: A): M

    fun modelToApiEntity(model: M): A
    fun modelToEntity(model: M): E

    fun apiEntityListToModelList(entities: List<A>): List<M>

    fun modelListToApiEntityList(models: List<M>): List<A>

    fun apiEntityToEntity(entity: A): E

    fun apiEntityListToEntityList(entities: List<A>): List<E>

    fun entityListToModelList(entities: List<E>): List<M>

}