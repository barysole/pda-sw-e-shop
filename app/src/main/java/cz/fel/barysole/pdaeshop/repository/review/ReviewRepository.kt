package cz.fel.barysole.pdaeshop.repository.review

import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import io.reactivex.rxjava3.core.Single

interface ReviewRepository {

    fun loadReviewListFor(productId: String): Single<Result>

    fun addReview(inputReview: ReviewModel): Single<Result>

    class Result(val userError: UserError, val list: List<ReviewModel>) {
        val isSuccessful: Boolean
            get() = userError === UserError.NO_ERROR
    }

}