package cz.fel.barysole.pdaeshop.ui.cart.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import java.math.BigDecimal
import javax.inject.Inject

class ProductsInCartAdapter @Inject constructor() :
    BaseRecyclerAdapter<ProductsInCartAdapter.ProductInCartViewHolder>() {

    private var items = arrayListOf<ProductInCartModel>()
    var onProductInCartItemClickListener: ((productInCart: ProductInCartModel, position: Int) -> Unit)? =
        null
    var onProductItemDeleteClickListener: ((productInCart: ProductInCartModel, position: Int) -> Unit)? =
        null

    inner class ProductInCartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val productImage: ImageView = itemView.findViewById(R.id.product_in_cart_image)
        private val productTitle: TextView = itemView.findViewById(R.id.product_in_cart_title)
        private val productPrice: TextView = itemView.findViewById(R.id.product_in_cart_price)
        private val productAmount: TextView = itemView.findViewById(R.id.product_in_cart_amount)
        private val deleteFromCart: MaterialButton =
            itemView.findViewById(R.id.product_in_cart_delete)

        init {
            view.setOnClickListener {
                onProductInCartItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(productInCart: ProductInCartModel, position: Int) {
            ImageLoader(
                productImage,
                R.drawable.e_shop_logo_icon_with_padding,
                productInCart.product.imageUrl
            ).load()
            productTitle.text = productInCart.product.title
            productPrice.text =
                (productInCart.product.price.multiply(BigDecimal(productInCart.amount))).toString() + " Kč"
            productAmount.text = productInCart.amount.toString() + "x"
            deleteFromCart.setOnClickListener {
                onProductItemDeleteClickListener?.let { it1 ->
                    it1(
                        productInCart,
                        position
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductInCartViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_in_cart, parent, false)
        return ProductInCartViewHolder(view)
    }


    override fun onBindViewHolder(holder: ProductInCartViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ProductInCartModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}