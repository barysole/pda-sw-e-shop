package cz.fel.barysole.pdaeshop.ui.shopdetail.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopServiceModel
import javax.inject.Inject

class ShopServiceAdapter @Inject constructor() :
    BaseRecyclerAdapter<ShopServiceAdapter.ShopServiceViewHolder>() {

    private var items = arrayListOf<ShopServiceModel>()

    var onServiceItemClickListener: ((product: ShopServiceModel, position: Int) -> Unit)? = null


    /* ViewHolder for displaying header. */

    inner class ShopServiceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val shopServiceIcon: ImageView = itemView.findViewById(R.id.service_icon)

        init {
            view.setOnClickListener {
                onServiceItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        fun bind(shopService: ShopServiceModel) {
            when (shopService.id) {
                "afc2a867-1353-4b8f-ae5f-6203018f47d2" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_local_parking_24
                        )
                    )
                }

                "15f0d2ae-fa57-4eaf-abd6-413123b8e81d" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_local_bar_24
                        )
                    )
                }

                "c4b0633a-d1a2-4f43-9765-b60f247fb3cc" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_local_cafe_24
                        )
                    )
                }

                "f1b65f79-3ab2-4deb-973c-ea0ae69c914d" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_family_restroom_24
                        )
                    )
                }

                "eeea5a1c-77b2-49d1-bc28-56c2f54f8646" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_atm_24
                        )
                    )
                }

                "6974e67a-e8a6-4301-b638-76da2e0e4060" -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_local_pharmacy_24
                        )
                    )
                }

                else -> {
                    shopServiceIcon.setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.outline_add_photo_alternate_24
                        )
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopServiceViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_service, parent, false)
        return ShopServiceViewHolder(view)
    }


    override fun onBindViewHolder(holder: ShopServiceViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ShopServiceModel>) {
        this.items = items
        notifyDataSetChanged()
    }

}