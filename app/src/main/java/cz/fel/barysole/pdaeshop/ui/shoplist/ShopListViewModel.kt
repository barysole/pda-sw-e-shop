package cz.fel.barysole.pdaeshop.ui.shoplist

import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.shopandservice.ShopAndServiceRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ShopListViewModel @Inject constructor(
    private val shopAndServiceRepository: ShopAndServiceRepository
) :
    BaseViewModel() {

    val chosenShopLiveData = MutableLiveData<ShopModel?>()
    val shopListLiveData = MutableLiveData<List<ShopModel>>()
    val isLoadingLiveData = MutableLiveData<Boolean>()
    val noDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()

    private var loadShopListDisposable = Disposable.disposed()

    fun loadShopList() {
        loadShopListDisposable.dispose()
        loadShopListDisposable = shopAndServiceRepository.loadShopList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    shopListLiveData.value = result.list
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noDataLiveData.value = result.list.isEmpty()
                isLoadingLiveData.value = false
            }, {
                isLoadingLiveData.value = false
                noDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun onChooseShop(chosenShop: ShopModel?) {
        chosenShopLiveData.value = chosenShop
    }

    override fun onStart() {
        loadShopList()
    }

    override fun onCleared() {
        loadShopListDisposable.dispose()
        super.onCleared()
    }
}