package cz.fel.barysole.pdaeshop.repository.shopandservice

import cz.fel.barysole.pdaeshop.api.EshopServerApi
import cz.fel.barysole.pdaeshop.db.room.dao.ShopAndServiceDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopServiceDao
import cz.fel.barysole.pdaeshop.db.room.model.ShopAndServiceEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopServicesEntity
import cz.fel.barysole.pdaeshop.repository.error.RepositoryException
import cz.fel.barysole.pdaeshop.repository.error.SimpleUserError
import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.repository.mapper.ShopMapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShopAndServiceRepositoryImpl @Inject constructor(
    private val shopDao: ShopDao,
    private val shopAndServiceDao: ShopAndServiceDao,
    private val shopServiceDao: ShopServiceDao,
    private val eshopApi: EshopServerApi,
    private val shopMapper: ShopMapper
) : ShopAndServiceRepository {

    override fun loadShopList(): Single<ShopAndServiceRepository.Result> {
        return eshopApi.getAllShop()
            .doOnSuccess { response ->
                if (response.isSuccessful) {
                    val shopList = response.body()!!.shopList
                    shopDao.deleteAll()
                    for (shop in shopList) {
                        shopDao.insert(shopMapper.apiEntityToEntity(shop))
                        if (shop.serviceList != null) {
                            for (service in shop.serviceList) {
                                val shopServiceEntity = ShopServicesEntity()
                                shopServiceEntity.serviceId = service.id
                                shopServiceEntity.name = service.name ?: ""
                                shopServiceDao.insert(shopServiceEntity)
                                val shopAndServiceConnection = ShopAndServiceEntity()
                                shopAndServiceConnection.serviceId = service.id
                                shopAndServiceConnection.shopId = shop.id
                                shopAndServiceDao.insert(shopAndServiceConnection)
                            }
                        }
                    }
                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .flatMap { Single.fromCallable { shopDao.getShopListWithServices() } }
            .map { shopWithServicesEntityList ->
                val shopList =
                    shopMapper.shopWithServicesListToShopModelList(shopWithServicesEntityList)
                ShopAndServiceRepository.Result(UserError.NO_ERROR, shopList)
            }
            .onErrorReturn { error ->
                try {
                    ShopAndServiceRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ),
                        shopMapper.shopWithServicesListToShopModelList(shopDao.getShopListWithServices())
                    )
                } catch (e: Exception) {
                    ShopAndServiceRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), emptyList()
                    )
                }
            }
    }

    override fun loadShop(shopId: String): Single<ShopAndServiceRepository.Result> {
        return eshopApi.getShopWithId(shopId)
            .doOnSuccess { response ->
                if (response.isSuccessful) {
                    val shop = response.body()!!
                    if (shopDao.getShopWithServices(shop.id) != null) {
                        shopDao.update(shopMapper.apiEntityToEntity(shop))
                        if (shop.serviceList != null) {
                            for (service in shop.serviceList) {
                                val shopServiceEntity = ShopServicesEntity()
                                shopServiceEntity.serviceId = service.id
                                shopServiceEntity.name = service.name ?: ""
                                shopServiceDao.insert(shopServiceEntity)
                                val shopAndServiceConnection = ShopAndServiceEntity()
                                shopAndServiceConnection.serviceId = service.id
                                shopAndServiceConnection.shopId = shop.id
                                shopAndServiceDao.insert(shopAndServiceConnection)
                            }
                        }
                    } else {
                        shopDao.insert(shopMapper.apiEntityToEntity(shop))
                        if (shop.serviceList != null) {
                            for (service in shop.serviceList) {
                                val shopServiceEntity = ShopServicesEntity()
                                shopServiceEntity.serviceId = service.id
                                shopServiceEntity.name = service.name ?: ""
                                shopServiceDao.insert(shopServiceEntity)
                                val shopAndServiceConnection = ShopAndServiceEntity()
                                shopAndServiceConnection.serviceId = service.id
                                shopAndServiceConnection.shopId = shop.id
                                shopAndServiceDao.insert(shopAndServiceConnection)
                            }
                        }
                    }
                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .flatMap { Single.fromCallable { shopDao.getShopWithServices(shopId)!! } }
            .map { shopWithServices ->
                val shop = shopMapper.shopWithServicesToShopModel(shopWithServices)
                ShopAndServiceRepository.Result(UserError.NO_ERROR, listOf(shop))
            }
            .onErrorReturn { error ->
                try {
                    ShopAndServiceRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ),
                        listOf(
                            shopMapper.shopWithServicesToShopModel(
                                shopDao.getShopWithServices(shopId)!!
                            )
                        )
                    )
                } catch (e: Exception) {
                    ShopAndServiceRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), emptyList()
                    )
                }
            }
    }

}