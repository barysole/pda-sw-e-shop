package cz.fel.barysole.pdaeshop.repository.shopandservice

import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import io.reactivex.rxjava3.core.Single

interface ShopAndServiceRepository {

    fun loadShopList(): Single<Result>

    fun loadShop(shopId: String): Single<Result>

    class Result(val userError: UserError, val list: List<ShopModel>) {
        val isSuccessful: Boolean
            get() = userError === UserError.NO_ERROR
    }

}