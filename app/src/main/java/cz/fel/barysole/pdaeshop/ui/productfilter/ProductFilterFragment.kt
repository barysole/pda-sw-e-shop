package cz.fel.barysole.pdaeshop.ui.productfilter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cz.fel.barysole.pdaeshop.databinding.FragmentProductFilterBinding
import cz.fel.barysole.pdaeshop.ui.productfilter.adapter.ProductsTypeAdapter
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProductFilterFragment : Fragment() {

    private var _binding: FragmentProductFilterBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var noDataTextView: TextView

    @Inject
    lateinit var adapter: ProductsTypeAdapter

    private val productTypeViewModel by viewModels<ProductFilterViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductFilterBinding.inflate(inflater, container, false)
        val root: View = binding.root

        noDataTextView = binding.noDataText
        recyclerView = binding.productFilterRV
        recyclerView.adapter =
            adapter.apply {
                onProductTypeItemClickListener = { productType, position ->
                    productTypeViewModel.onFilterItemClick(productType, position)
                }
            }

        productTypeViewModel.apply {
            productsTypeLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { products -> drawProductsType(products) })
            noDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> showError(noData) })
            changedProductTypeLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { pair -> changeProductType(pair) })
            loadProductsType()
            start()
        }

        return root
    }

    private fun drawProductsType(items: List<ProductTypeModel>) {
        adapter.setItems(ArrayList(items))
    }

    private fun changeProductType(item: Pair<ProductTypeModel, Int>) {
        adapter.setItem(item.first, item.second)
    }

    private fun showError(error: String) {
        activity?.apply {
            Snackbar.make(this.findViewById(android.R.id.content), error, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    private fun setNoData(noData: Boolean) {
        if (noData) {
            noDataTextView.visibility = View.VISIBLE
            recyclerView.visibility = View.INVISIBLE
        } else {
            noDataTextView.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

}