package cz.fel.barysole.pdaeshop.worker

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import cz.fel.barysole.pdaeshop.EshopApp.Companion.CHANNEL_ID
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers


@HiltWorker
class ProductNotificationWorker @AssistedInject constructor(
    @Assisted var context: Context, @Assisted var params: WorkerParameters,
    var cartRepository: CartRepository
) : Worker(context, params) {

    private var loadProductsInCartDisposable = Disposable.disposed()

    override fun doWork(): Result {
        loadProductsInCartDisposable = cartRepository.loadProductsInCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (result.isSuccessful && result.list.isNotEmpty()) {
                    var builder = NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.outline_shopping_cart_24)
                        .setContentTitle("Products are waiting...")
                        .setContentText("You have some products in your cart. Please don't forget them!")
                        .setStyle(
                            NotificationCompat.BigTextStyle()
                                .bigText("You have some products in your cart. Please don't forget them!")
                        )
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    with(NotificationManagerCompat.from(context)) {
                        notify(builder.hashCode(), builder.build())
                    }
                }
                if (!result.isSuccessful) {
                    //ignore
                }
            }, {
                //ignore
            })
        return Result.success()
    }


}