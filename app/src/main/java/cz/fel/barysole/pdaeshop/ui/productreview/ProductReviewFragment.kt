package cz.fel.barysole.pdaeshop.ui.productreview

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentProductReviewBinding
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.base.BaseItemDecoration
import cz.fel.barysole.pdaeshop.ui.productreview.adapter.ReviewPhotoAdapter
import cz.fel.barysole.pdaeshop.ui.productreview.adapter.ReviewPhotoItem
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import javax.inject.Inject

@AndroidEntryPoint
class ProductReviewFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var authorNameInput: TextInputLayout
    private lateinit var advantagesInput: TextInputLayout
    private lateinit var disadvantagesInput: TextInputLayout

    private lateinit var productRating1: MaterialButton
    private lateinit var productRating2: MaterialButton
    private lateinit var productRating3: MaterialButton
    private lateinit var productRating4: MaterialButton
    private lateinit var productRating5: MaterialButton
    private lateinit var sendReviewButton: Button

    private var _binding: FragmentProductReviewBinding? = null
    private lateinit var navController: NavController

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    val args: ProductReviewFragmentArgs by navArgs()

    @Inject
    lateinit var adapter: ReviewPhotoAdapter

    private lateinit var activityForResultLauncher: ActivityResultLauncher<Intent>

    private val productReviewViewModel by viewModels<ProductReviewViewModel>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        registerActivityForResult()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductReviewBinding.inflate(inflater, container, false)
        val root: View = binding.root
        //base
        loaderHolder = binding.loaderHolder
        loaderIcon = binding.loaderIcon
        navController = findNavController()

        recyclerView = binding.reviewPhotoRw
        recyclerView.adapter = adapter.apply {
            onAddPhotoItemClickListener = { _ ->
                addPhotoClick()
            }
        }

        recyclerView.addItemDecoration(
            BaseItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.review_photo_rw_margin)
            )
        )
        productRating1 = binding.productRating1
        productRating1.setOnClickListener { productReviewViewModel.onProductRatingChanges(1) }
        productRating2 = binding.productRating2
        productRating2.setOnClickListener { productReviewViewModel.onProductRatingChanges(2) }
        productRating3 = binding.productRating3
        productRating3.setOnClickListener { productReviewViewModel.onProductRatingChanges(3) }
        productRating4 = binding.productRating4
        productRating4.setOnClickListener { productReviewViewModel.onProductRatingChanges(4) }
        productRating5 = binding.productRating5
        productRating5.setOnClickListener { productReviewViewModel.onProductRatingChanges(5) }
        authorNameInput = binding.reviewMyName
        advantagesInput = binding.reviewAdvantages
        disadvantagesInput = binding.reviewDisadvantages
        sendReviewButton = binding.sendReviewButton
        sendReviewButton.setOnClickListener {
            sendReview()
        }

        (activity as AppCompatActivity).supportActionBar?.subtitle = args.productName

        productReviewViewModel.apply {
            reviewPhotoItemListLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { products -> drawReviewPhotoItemList(products) })
            isLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> showError(noData) })
            isReviewSentLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isReviewSent -> if (isReviewSent) showSuccessNotificationAndReturn() })
            productRatingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { rating -> changeProductRating(rating) })
            start()
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
    }

    private fun drawReviewPhotoItemList(itemList: List<ReviewPhotoItem>) {
        adapter.setItems(ArrayList(itemList))
    }

    private fun registerActivityForResult() {
        activityForResultLauncher =
            registerForActivityResult(
                ActivityResultContracts.StartActivityForResult()
            ) {
                if (it.resultCode == Activity.RESULT_OK) {
                    val photoBitmap = it.data?.extras?.get("data") as Bitmap
                    productReviewViewModel.onReviewPhotoTaken(photoBitmap)
                }
            }
    }

    private fun changeProductRating(productRating: Int) {
        var tempRating = BigDecimal(productRating)
        val startList = mutableListOf(
            productRating1,
            productRating2,
            productRating3,
            productRating4,
            productRating5
        )
        while (tempRating > BigDecimal.ZERO) {
            if (tempRating >= BigDecimal.ONE) {
                startList[0].setIcon(
                    AppCompatResources.getDrawable(
                        requireContext(),
                        R.drawable.baseline_star_24
                    )
                )
                startList.removeAt(0)
                tempRating = tempRating.minus(BigDecimal.ONE)
            } else if (tempRating > BigDecimal.ZERO) {
                startList[0].setIcon(
                    AppCompatResources.getDrawable(
                        requireContext(),
                        R.drawable.baseline_star_half_24
                    )
                )
                startList.removeAt(0)
                tempRating = BigDecimal.ZERO
            }
        }
        for (remainingStar in startList) {
            remainingStar.setIcon(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.baseline_star_border_24
                )
            )
        }
    }

    private fun showSuccessNotificationAndReturn() {
        activity?.apply {
            Snackbar.make(
                this.findViewById(android.R.id.content),
                "Review has been sent.",
                Snackbar.LENGTH_LONG
            ).show()
        }
        navController.navigateUp()
    }

    private fun sendReview() {
        if (authorNameInput.editText?.text.toString().isBlank()) {
            MaterialAlertDialogBuilder(
                requireContext(),
                R.style.ThemeOverlay_App_MaterialAlertDialog
            )
                .setTitle(resources.getString(R.string.validation_failed_title))
                .setMessage(resources.getString(R.string.validation_author_failed_text))
                .setPositiveButton(resources.getString(R.string.ok_text)) { dialog, which ->
                }
                .show()
        } else {
            productReviewViewModel.onReviewSend(
                authorNameInput.editText?.text.toString(),
                advantagesInput.editText?.text.toString(),
                disadvantagesInput.editText?.text.toString(),
                args.productId
            )
        }
    }

    private fun addPhotoClick() {
        if (context?.packageManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) == true) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            activityForResultLauncher.launch(takePictureIntent)
        } else {
            showError("No camera has been detected!")
        }
    }

}