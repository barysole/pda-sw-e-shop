package cz.fel.barysole.pdaeshop.api

import cz.fel.barysole.pdaeshop.api.model.products.ProductsApiEntity
import cz.fel.barysole.pdaeshop.api.model.review.ReviewApiEntity
import cz.fel.barysole.pdaeshop.api.model.shop.ShopApiEntity
import cz.fel.barysole.pdaeshop.api.request.AddReviewRequest
import cz.fel.barysole.pdaeshop.api.response.ProductListResponse
import cz.fel.barysole.pdaeshop.api.response.ReviewResponse
import cz.fel.barysole.pdaeshop.api.response.ShopListResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface EshopServerApi {

    @GET("productCatalog/list")
    fun getAllProducts(): Single<Response<ProductListResponse>>

    @GET("productCatalog/{id}/get")
    fun getProductWithId(
        @Path(
            value = "id",
            encoded = false
        ) id: String
    ): Single<Response<ProductsApiEntity>>

    @GET("reviews/getForProduct/{id}/all")
    fun getReviewListForProduct(@Path("id") id: String): Single<Response<ReviewResponse>>

    @PUT("reviews/add")
    fun addReview(@Body reviewRequest: AddReviewRequest): Single<Response<ReviewApiEntity>>

    @GET("shop/list")
    fun getAllShop(): Single<Response<ShopListResponse>>

    @GET("shop/{id}/get")
    fun getShopWithId(
        @Path(
            value = "id",
            encoded = false
        ) id: String
    ): Single<Response<ShopApiEntity>>

}