package cz.fel.barysole.pdaeshop.ui.base

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

class ImageLoader(
    private val imageView: ImageView,
    @DrawableRes private val placeholder: Int,
    private val imageUrl: String?,
    private val resizedDimension: Pair<Int, Int>? = null,
    private val successListener: (() -> Unit)? = null,
    private val isCenterCrop: Boolean = false
) {

    private var listener: RequestListener<Drawable>? = null

    init {
        successListener?.let {
            listener = object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    successListener.invoke()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    successListener.invoke()
                    return false
                }
            }
        }
    }

    private val requestOptions
        get() = if (resizedDimension == null) {
            if (isCenterCrop) {
                RequestOptions
                    .diskCacheStrategyOf(DiskCacheStrategy.DATA)
                    .placeholder(placeholder)
                    .centerCrop()
                    .override(imageView.width, imageView.height)
            } else {
                RequestOptions
                    .diskCacheStrategyOf(DiskCacheStrategy.DATA)
                    .placeholder(placeholder)
                    .override(imageView.width, imageView.height)
            }

        } else {
            if (isCenterCrop) {
                RequestOptions
                    .diskCacheStrategyOf(DiskCacheStrategy.DATA)
                    .placeholder(placeholder)
                    .centerCrop()
                    .override(resizedDimension.first, resizedDimension.second)
            } else {
                RequestOptions
                    .diskCacheStrategyOf(DiskCacheStrategy.DATA)
                    .placeholder(placeholder)
                    .override(resizedDimension.first, resizedDimension.second)
            }
        }


    fun load() {
        onImageChanged()
    }

    private fun onImageChanged() {
        Glide.with(imageView)
            .load(imageUrl)
            .apply(requestOptions)
            .addListener(listener)
            .into(imageView)
    }

    companion object {
        fun clear(view: View) {
            Glide.with(view)
                .clear(view)
        }
    }

}