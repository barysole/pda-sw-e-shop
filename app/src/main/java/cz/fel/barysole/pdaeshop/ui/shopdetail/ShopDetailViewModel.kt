package cz.fel.barysole.pdaeshop.ui.shopdetail

import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.shopandservice.ShopAndServiceRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ShopDetailViewModel @Inject constructor(
    private val shopAndServiceRepository: ShopAndServiceRepository
) :
    BaseViewModel() {

    private var currentShopId: String = ""

    val shopLiveData = MutableLiveData<ShopModel>()
    val isShopLoadingLiveData = MutableLiveData<Boolean>()
    val noShopDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()

    private var loadShopDisposable = Disposable.disposed()

    fun loadShop(shopId: String) {
        loadShopDisposable = shopAndServiceRepository.loadShop(shopId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isShopLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    shopLiveData.value = result.list[0]
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noShopDataLiveData.value = result.list.isEmpty()
                isShopLoadingLiveData.value = false
            }, {
                isShopLoadingLiveData.value = false
                noShopDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun setCurrentShopId(shopId: String) {
        currentShopId = shopId
    }

    override fun onStart() {
        /* if (currentShopId.isNotEmpty()) {
             loadShop(currentProductId)
         }*/
    }

    override fun onCleared() {
        loadShopDisposable.dispose()
        super.onCleared()
    }
}