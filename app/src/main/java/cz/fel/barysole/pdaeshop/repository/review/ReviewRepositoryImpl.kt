package cz.fel.barysole.pdaeshop.repository.review

import cz.fel.barysole.pdaeshop.api.EshopServerApi
import cz.fel.barysole.pdaeshop.api.request.AddReviewRequest
import cz.fel.barysole.pdaeshop.db.room.dao.ReviewDao
import cz.fel.barysole.pdaeshop.db.room.model.ReviewEntity
import cz.fel.barysole.pdaeshop.repository.error.RepositoryException
import cz.fel.barysole.pdaeshop.repository.error.SimpleUserError
import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.repository.mapper.ReviewMapper
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReviewRepositoryImpl @Inject constructor(
    private val reviewDao: ReviewDao,
    private val reviewApi: EshopServerApi,
    private val reviewMapper: ReviewMapper
) : ReviewRepository {

    override fun loadReviewListFor(productId: String): Single<ReviewRepository.Result> {
        return reviewApi.getReviewListForProduct(productId)
            .doOnSuccess { response ->
                if (response.isSuccessful) {
                    val reviewList = response.body()!!.reviewList
                    reviewDao.deleteAll()
                    for (review in reviewList) {
                        reviewDao.insert(reviewMapper.apiEntityToEntity(review))
                    }
                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .flatMap { Single.fromCallable { reviewDao.getByProductId(productId) } }
            .map { reviewEntityList ->
                val reviewList = reviewMapper.entityListToModelList(reviewEntityList)
                ReviewRepository.Result(UserError.NO_ERROR, reviewList)
            }
            .onErrorReturn { error ->
                try {
                    ReviewRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), reviewMapper.entityListToModelList(reviewDao.getByProductId(productId))
                    )
                } catch (e: Exception) {
                    ReviewRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), emptyList()
                    )
                }
            }
    }

    override fun addReview(inputReview: ReviewModel): Single<ReviewRepository.Result> {
        val addReviewRequest = AddReviewRequest()
        addReviewRequest.author = inputReview.author
        addReviewRequest.advantages = inputReview.advantages
        addReviewRequest.disadvantages = inputReview.disadvantages
        addReviewRequest.rating = inputReview.rating.toPlainString()
        addReviewRequest.product = AddReviewRequest.Product(inputReview.productId)
        return reviewApi.addReview(addReviewRequest)
            .map { response ->
                if (response.isSuccessful) {
                    val reviewEntityList = mutableListOf<ReviewEntity>()
                    val review = response.body()!!
                    val reviewId = reviewDao.insert(reviewMapper.apiEntityToEntity(review))
                    reviewEntityList.add(reviewDao.getById(reviewId.toString()))
                    return@map ReviewRepository.Result(
                        UserError.NO_ERROR,
                        reviewMapper.entityListToModelList(reviewEntityList)
                    )

                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .onErrorReturn { error ->
                ReviewRepository.Result(
                    SimpleUserError(
                        error.message ?: "Something went wrong..."
                    ), emptyList()
                )
            }
    }

}