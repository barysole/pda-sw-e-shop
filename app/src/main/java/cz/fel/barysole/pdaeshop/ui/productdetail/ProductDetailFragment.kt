package cz.fel.barysole.pdaeshop.ui.productdetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentProductDetailBinding
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.productdetail.adapter.ReviewAdapter
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import javax.inject.Inject


@AndroidEntryPoint
class ProductDetailFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var addToCartButton: Button
    private lateinit var productImage: ImageView
    private lateinit var productDetailPrice: TextView
    private lateinit var productRatingCount: TextView
    private lateinit var productRating1: ImageView
    private lateinit var productRating2: ImageView
    private lateinit var productRating3: ImageView
    private lateinit var productRating4: ImageView
    private lateinit var productRating5: ImageView
    private lateinit var productDescription: TextView
    private lateinit var writeReviewButton: Button
    private lateinit var scrollView: NestedScrollView
    private lateinit var reviewDataStatus: TextView

    private lateinit var navController: NavController

    @Inject
    lateinit var adapter: ReviewAdapter

    private var _binding: FragmentProductDetailBinding? = null
    private val args: ProductDetailFragmentArgs by navArgs()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val productsDetailViewModel by viewModels<ProductDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //base
        loaderHolder = binding.loaderHolder
        loaderIcon = binding.loaderIcon
        noDataButton = binding.noDataButton
        noDataTextView = binding.noDataText
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
        navController = findNavController()

        productImage = binding.productDetailImage
        productDetailPrice = binding.productDetailPrice
        productRatingCount = binding.productDetailRatingCount
        productRating1 = binding.productRating1
        productRating2 = binding.productRating2
        productRating3 = binding.productRating3
        productRating4 = binding.productRating4
        productRating5 = binding.productRating5
        productDescription = binding.productDetailDescription
        scrollView = binding.detailScrollView
        writeReviewButton = binding.writeReviewButton
        addToCartButton = binding.addToCartButton
        addToCartButton.setOnClickListener { productsDetailViewModel.onProductAddToCart() }
        recyclerView = binding.recyclerReviewList
        recyclerView.adapter = adapter
        reviewDataStatus = binding.reviewDataStatus

        productsDetailViewModel.apply {
            setCurrentProductId(args.productId)
            reviewLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { reviews -> drawReviewData(reviews) })
            isProductLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            noProductDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { error -> showError(error) })
            productLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { productData -> drawProductData(productData) })
            noReviewsDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> changeReviewDataStatus(noData) })
            productAddedSuccessfullyLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isAddedSuccess -> if (isAddedSuccess) showInfo("Product has been successfully added.") })
            start()
            loadProduct(args.productId)
            loadReviewList(args.productId)
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as AppCompatActivity).supportActionBar?.subtitle = ""
    }

    private fun changeReviewDataStatus(noData: Boolean = false) {
        if (noData) {
            reviewDataStatus.text = getString(R.string.review_status_nodata)
            reviewDataStatus.visibility = View.VISIBLE
        } else {
            reviewDataStatus.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    fun drawProductData(productData: ProductModel) {
        ImageLoader(
            productImage,
            R.drawable.e_shop_logo_icon_with_padding,
            productData.imageUrl
        ).load()
        (activity as AppCompatActivity).supportActionBar?.subtitle = productData.title
        productDetailPrice.text = productData.price.toString() + " Kč"
        productRatingCount.text = productData.ratingCount.toString() + "x"
        productDescription.text = productData.description

        writeReviewButton.setOnClickListener {
            val action =
                ProductDetailFragmentDirections.actionProductDetailFragmentToProductReviewFragment(
                    productData.title,
                    productData.id
                )
            navController.navigate(action)
        }

        var tempRating = productData.rating
        val startList = mutableListOf(
            productRating1,
            productRating2,
            productRating3,
            productRating4,
            productRating5
        )
        while (tempRating > BigDecimal.ZERO) {
            if (tempRating >= BigDecimal.ONE) {
                startList[0].setImageDrawable(
                    AppCompatResources.getDrawable(
                        requireContext(),
                        R.drawable.baseline_star_24
                    )
                )
                startList.removeAt(0)
                tempRating = tempRating.minus(BigDecimal.ONE)
            } else if (tempRating > BigDecimal.ZERO) {
                startList[0].setImageDrawable(
                    AppCompatResources.getDrawable(
                        requireContext(),
                        R.drawable.baseline_star_half_24
                    )
                )
                startList.removeAt(0)
                tempRating = BigDecimal.ZERO
            }
        }
        for (remainingStar in startList) {
            remainingStar.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.baseline_star_border_24
                )
            )
        }
    }

    private fun drawReviewData(reviewList: List<ReviewModel>) {
        adapter.setItems(ArrayList(reviewList))
    }

    override fun setLoading(isLoading: Boolean) {
        super.setLoading(isLoading)
        if (isLoading) {
            scrollView.visibility = View.INVISIBLE
            writeReviewButton.visibility = View.GONE
            addToCartButton.visibility = View.GONE
        } else {
            scrollView.visibility = View.VISIBLE
            writeReviewButton.visibility = View.VISIBLE
            addToCartButton.visibility = View.VISIBLE
        }
    }

    override fun setNoData(noData: Boolean) {
        super.setNoData(noData)
        if (noData) {
            scrollView.visibility = View.INVISIBLE
            writeReviewButton.visibility = View.GONE
            addToCartButton.visibility = View.GONE
        } else {
            scrollView.visibility = View.VISIBLE
            writeReviewButton.visibility = View.VISIBLE
            addToCartButton.visibility = View.VISIBLE
        }
    }

}