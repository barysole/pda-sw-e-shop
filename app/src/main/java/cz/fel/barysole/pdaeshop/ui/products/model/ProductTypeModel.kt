package cz.fel.barysole.pdaeshop.ui.products.model

class ProductTypeModel {

    var id: String = ""
    var name: String = ""
    var isInFilter: Boolean = true

    override fun equals(other: Any?): Boolean {
        return if (other is ProductTypeModel) {
            id == other.id
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}