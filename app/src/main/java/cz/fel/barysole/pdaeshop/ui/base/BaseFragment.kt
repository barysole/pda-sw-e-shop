package cz.fel.barysole.pdaeshop.ui.base

import android.graphics.drawable.AnimationDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment() {

    protected var loaderHolder: ViewGroup? = null
    protected var loaderIcon: ImageView? = null
    protected var noDataButton: Button? = null
    protected var noDataTextView: TextView? = null

    protected fun showError(error: String) {
        activity?.apply {
            Snackbar.make(this.findViewById(android.R.id.content), error, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    protected fun showInfo(info: String) {
        activity?.apply {
            Snackbar.make(this.findViewById(android.R.id.content), info, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    protected open fun setLoading(isLoading: Boolean) {
        if (isLoading) {
            loaderHolder?.visibility = View.VISIBLE
            (loaderIcon?.drawable as AnimationDrawable).start()
        } else {
            (loaderIcon?.drawable as AnimationDrawable).stop()
            loaderHolder?.visibility = View.GONE
        }
    }

    protected open fun setNoData(noData: Boolean) {
        if (noData) {
            noDataTextView?.visibility = View.VISIBLE
            noDataButton?.visibility = View.VISIBLE
        } else {
            noDataTextView?.visibility = View.GONE
            noDataButton?.visibility = View.GONE
        }
    }

}