package cz.fel.barysole.pdaeshop.ui.cart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val cartRepository: CartRepository) : ViewModel() {

    val productsInCartLiveData = MutableLiveData<List<ProductInCartModel>>()
    val noDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()
    val chosenProductLiveData = MutableLiveData<ProductInCartModel?>()

    private var loadProductsInCartDisposable = Disposable.disposed()
    private var changeProductAmountDisposable = Disposable.disposed()

    fun loadProductsInCart() {
        loadProductsInCartDisposable = cartRepository.loadProductsInCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (result.isSuccessful) {
                    productsInCartLiveData.value = result.list
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noDataLiveData.value = result.list.isEmpty()
            }, {
                noDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun onChoseProduct(productInCart: ProductInCartModel?) {
        chosenProductLiveData.value = productInCart
    }

    fun onChangeProductAmount(productAmount: Int, product: ProductInCartModel? = null) {
        val chosenProduct = product ?: chosenProductLiveData.value
        if (chosenProduct != null) {
            chosenProduct.amount = productAmount
            changeProductAmountDisposable = cartRepository.updateProductInCart(chosenProduct)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.message.isEmpty()) {
                        loadProductsInCart()
                        chosenProductLiveData.value = null
                    } else {
                        errorLiveData.value = result.message
                    }
                }, {
                    errorLiveData.value = it.message
                })
        }
    }

    override fun onCleared() {
        loadProductsInCartDisposable.dispose()
        super.onCleared()
    }

}