package cz.fel.barysole.pdaeshop.repository.error

interface UserError {
    val message: String

    companion object {
        val NO_ERROR: UserError = object : UserError {
            override val message = ""
        }
    }
}