package cz.fel.barysole.pdaeshop.repository.mapper

import cz.fel.barysole.pdaeshop.api.model.products.ProductTypeApiEntity
import cz.fel.barysole.pdaeshop.api.model.products.ProductsApiEntity
import cz.fel.barysole.pdaeshop.db.room.model.ProductEntity
import cz.fel.barysole.pdaeshop.db.room.model.ProductTypeEntity
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import org.mapstruct.Mapper

@Mapper
abstract class ProductMapper {

    //product mapping
    fun apiEntityToEntity(entity: ProductsApiEntity): ProductEntity {
        val productEntity = ProductEntity()
        productEntity.id = entity.id
        productEntity.title = entity.title
        productEntity.description = entity.description
        productEntity.imageUrl = entity.imageUrl
        productEntity.rating = entity.rating
        productEntity.ratingCount = entity.ratingCount
        productEntity.price = entity.price
        productEntity.discount = entity.discount
        productEntity.productTypeId = entity.productType?.id ?: ""
        return productEntity
    }

    abstract fun apiEntityListToEntityList(entities: List<ProductsApiEntity>): List<ProductEntity>

    fun entityToModel(entity: ProductEntity): ProductModel {
        val productModel = ProductModel()
        productModel.id = entity.id
        productModel.title = entity.title
        productModel.description = entity.description
        productModel.imageUrl = entity.imageUrl
        productModel.rating = entity.rating
        productModel.ratingCount = entity.ratingCount
        productModel.price = entity.price
        productModel.discount = entity.discount
        val productType = ProductTypeModel()
        productType.id = entity.productTypeId
        productModel.productType = productType
        return productModel
    }

    abstract fun entityListToModel(entities: List<ProductEntity>): List<ProductModel>

    //product type mapping
    abstract fun productTypeModelToEntity(entity: ProductTypeModel): ProductTypeEntity
    abstract fun productTypeEntityToModel(entity: ProductTypeEntity): ProductTypeModel

    abstract fun productTypeApiEntityToEntity(entity: ProductTypeApiEntity): ProductTypeEntity

    abstract fun productTypeEntityListToModelList(entities: List<ProductTypeEntity>): List<ProductTypeModel>

}