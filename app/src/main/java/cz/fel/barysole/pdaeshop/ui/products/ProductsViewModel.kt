package cz.fel.barysole.pdaeshop.ui.products

import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import cz.fel.barysole.pdaeshop.repository.products.ProductRepository
import cz.fel.barysole.pdaeshop.repository.products.ProductTypeRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val cartRepository: CartRepository,
    private val productTypeRepository: ProductTypeRepository
) :
    BaseViewModel() {

    val productsLiveData = MutableLiveData<List<ProductModel>>()
    val isLoadingLiveData = MutableLiveData<Boolean>()
    val noDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()
    val productAddedSuccessfullyLiveData = SingleEventLiveData<Boolean>()

    private var filters: List<ProductTypeModel>? = null
    private var loadProductsDisposable = Disposable.disposed()
    private var loadProductsTypesDisposable = Disposable.disposed()
    private var addProductDisposable = Disposable.disposed()


    private fun loadProducts(withFilters: Boolean) {
        loadProductsDisposable.dispose()
        loadProductsDisposable = productRepository.loadProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    if (withFilters && filters != null) {
                        productsLiveData.value =
                            result.list.filter { product -> filters!!.contains(product.productType) }
                    } else {
                        productsLiveData.value = result.list
                    }
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noDataLiveData.value = result.list.isEmpty()
                isLoadingLiveData.value = false
            }, {
                isLoadingLiveData.value = false
                noDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun loadProductsWithFilters(requiredReload: Boolean) {
        loadProductsTypesDisposable.dispose()
        loadProductsTypesDisposable = productTypeRepository.loadProductsType()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (result.isSuccessful) {
                    val tempFilters = result.list?.filter { productType -> productType.isInFilter }
                    if (result.list?.isEmpty() == true) {
                        loadProducts(false)
                    } else if (filters != tempFilters) {
                        filters = tempFilters ?: mutableListOf()
                        loadProducts(true)
                    } else if (requiredReload) {
                        loadProducts(true)
                    }
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                    loadProducts(false)
                }
            }, {
                errorLiveData.value = it.message
            })
    }

    fun onProductAddToCart(product: ProductModel) {
        addProductDisposable = cartRepository.saveProductToCart(product)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (result.message.isEmpty()) {
                    productAddedSuccessfullyLiveData.value = true
                } else {
                    errorLiveData.value = result.message
                }
            }, {
                errorLiveData.value = it.message
            })
    }

    override fun onStart() {

    }

    override fun onCleared() {
        loadProductsDisposable.dispose()
        loadProductsTypesDisposable.dispose()
        super.onCleared()
    }
}