package cz.fel.barysole.pdaeshop.ui.productreview

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.review.ReviewRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.productreview.adapter.ReviewPhotoAddItem
import cz.fel.barysole.pdaeshop.ui.productreview.adapter.ReviewPhotoImageItem
import cz.fel.barysole.pdaeshop.ui.productreview.adapter.ReviewPhotoItem
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class ProductReviewViewModel @Inject constructor(private val reviewRepository: ReviewRepository) :
    BaseViewModel() {

    val reviewPhotoItemListLiveData = MutableLiveData<List<ReviewPhotoItem>>()
    val isLoadingLiveData = MutableLiveData<Boolean>()
    val productRatingLiveData = MutableLiveData<Int>()
    val errorLiveData = SingleEventLiveData<String>()
    val isReviewSentLiveData = SingleEventLiveData<Boolean>()

    private var sendReviewDisposable = Disposable.disposed()

    override fun onStart() {
        reviewPhotoItemListLiveData.value = listOf(ReviewPhotoAddItem())
    }

    fun onReviewPhotoTaken(photo: Bitmap) {
        val photoItemList = reviewPhotoItemListLiveData.value?.toMutableList()
        photoItemList?.add(photoItemList.lastIndex, ReviewPhotoImageItem(photo))
        photoItemList.apply {
            reviewPhotoItemListLiveData.value = this
        }
    }

    fun onReviewSend(
        authorName: String,
        advantages: String,
        disadvantages: String,
        productId: String
    ) {
        val review = ReviewModel()
        review.author = authorName
        review.advantages = advantages
        review.disadvantages = disadvantages
        review.productId = productId
        review.rating = BigDecimal(productRatingLiveData.value ?: 0)
        sendReviewDisposable = reviewRepository.addReview(review)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty() && result.isSuccessful) {
                    isReviewSentLiveData.value = true
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                isLoadingLiveData.value = false
            }, {
                isLoadingLiveData.value = false
                errorLiveData.value = it.message
            })
    }

    fun onProductRatingChanges(productRating: Int) {
        productRatingLiveData.value = productRating
    }

    override fun onCleared() {
        super.onCleared()
        sendReviewDisposable.dispose()
    }

}