package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity

@Entity(primaryKeys = ["shopId", "serviceId"])
class ShopAndServiceEntity {

    var shopId: String = ""

    var serviceId: String = ""

}