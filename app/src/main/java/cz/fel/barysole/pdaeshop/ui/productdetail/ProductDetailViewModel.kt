package cz.fel.barysole.pdaeshop.ui.productdetail

import androidx.lifecycle.MutableLiveData
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import cz.fel.barysole.pdaeshop.repository.products.ProductRepository
import cz.fel.barysole.pdaeshop.repository.review.ReviewRepository
import cz.fel.barysole.pdaeshop.ui.base.BaseViewModel
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val productReviewRepository: ReviewRepository,
    private val cartRepository: CartRepository
) :
    BaseViewModel() {

    private var currentProductId: String = ""

    val productLiveData = MutableLiveData<ProductModel>()
    val reviewLiveData = MutableLiveData<List<ReviewModel>>()
    val isProductLoadingLiveData = MutableLiveData<Boolean>()
    val noProductDataLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleEventLiveData<String>()
    val isReviewListLoadingLiveData = MutableLiveData<Boolean>()
    val noReviewsDataLiveData = MutableLiveData<Boolean>()
    val productAddedSuccessfullyLiveData = SingleEventLiveData<Boolean>()

    private var loadProductDisposable = Disposable.disposed()
    private var addProductDisposable = Disposable.disposed()
    private var loadProductsTypesDisposable = Disposable.disposed()

    fun loadProduct(productId: String) {
        loadProductDisposable = productRepository.loadProduct(productId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isProductLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    productLiveData.value = result.list[0]
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noProductDataLiveData.value = result.list.isEmpty()
                isProductLoadingLiveData.value = false
            }, {
                isProductLoadingLiveData.value = false
                noProductDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun loadReviewList(productId: String) {
        loadProductsTypesDisposable = productReviewRepository.loadReviewListFor(productId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isReviewListLoadingLiveData.value = true }
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    reviewLiveData.value = result.list
                }
                if (!result.isSuccessful) {
                    errorLiveData.value = result.userError.message
                }
                noReviewsDataLiveData.value = result.list.isEmpty()
                isReviewListLoadingLiveData.value = false
            }, {
                isReviewListLoadingLiveData.value = false
                noReviewsDataLiveData.value = true
                errorLiveData.value = it.message
            })
    }

    fun onProductAddToCart() {
        if (productLiveData.value != null) {
            addProductDisposable = cartRepository.saveProductToCart(productLiveData.value!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.message.isEmpty()) {
                        productAddedSuccessfullyLiveData.value = true
                    } else {
                        errorLiveData.value = result.message
                    }
                }, {
                    errorLiveData.value = it.message
                })
        }
    }

    fun setCurrentProductId(productId: String) {
        currentProductId = productId
    }

    override fun onStart() {
        /* if (currentProductId.isNotEmpty()) {
             loadProduct(currentProductId)
         }*/
    }

    override fun onCleared() {
        loadProductDisposable.dispose()
        loadProductsTypesDisposable.dispose()
        super.onCleared()
    }
}