package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ProductTypeEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface ProductTypeDao : BaseDao<ProductTypeEntity> {

    @Query("SELECT * FROM ProductType WHERE id = :id")
    fun getById(id: String): ProductTypeEntity

    @Query("SELECT * FROM ProductType WHERE id = :id")
    fun getByIdRx(id: String): Flowable<ProductTypeEntity>

    @Query("SELECT * FROM ProductType")
    fun getAll(): List<ProductTypeEntity>

    @Query("SELECT * FROM ProductType")
    fun getAllRx(): Flowable<List<ProductTypeEntity>>

    @Query("DELETE FROM ProductType")
    fun deleteAll()
}