package cz.fel.barysole.pdaeshop.ui.personalinformation

import androidx.lifecycle.ViewModel
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import cz.fel.barysole.pdaeshop.ui.base.SingleEventLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class PersonalInformationViewModel @Inject constructor(private val cartRepository: CartRepository) :
    ViewModel() {

    val totalPriceAmountLiveData = SingleEventLiveData<String>()

    private var deleteProductsInCartDisposable = Disposable.disposed()
    private var getTotalPriceDisposable = Disposable.disposed()

    fun deleteProductsInCart() {
        deleteProductsInCartDisposable = cartRepository.clearTheCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //nothing
            }, {
                //nothing
            })
    }

    fun getTotalPrice() {
        getTotalPriceDisposable = cartRepository.loadProductsInCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                if (result.list.isNotEmpty()) {
                    var totalPrice = BigDecimal.ZERO;
                    for (product in result.list) {
                        totalPrice =
                            totalPrice.plus(product.product.price.multiply(product.amount.toBigDecimal()))
                    }
                    totalPriceAmountLiveData.value = totalPrice.toString()
                }
            }, {
                //nothing
            })
    }

    override fun onCleared() {
        super.onCleared()
        getTotalPriceDisposable.dispose()
    }

}