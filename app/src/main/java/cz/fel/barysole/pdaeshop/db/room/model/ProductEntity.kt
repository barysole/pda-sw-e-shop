package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import cz.fel.barysole.pdaeshop.db.room.converter.BigDecimalStringConverter
import java.math.BigDecimal

@Entity(tableName = "Product")
@TypeConverters(BigDecimalStringConverter::class)
class ProductEntity {
    @PrimaryKey
    var id: String = ""
    var title: String = ""
    var description: String = ""
    var imageUrl: String = ""
    var rating: BigDecimal = BigDecimal(0)
    var ratingCount: Int = 0
    var price: BigDecimal = BigDecimal(0)
    var discount: Int = 0
    var productTypeId: String = ""
}