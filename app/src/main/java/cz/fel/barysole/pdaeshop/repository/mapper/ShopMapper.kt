package cz.fel.barysole.pdaeshop.repository.mapper

import cz.fel.barysole.pdaeshop.api.model.shop.ShopApiEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopWithServices
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopServiceModel
import org.mapstruct.Mapper
import java.math.BigDecimal

@Mapper
abstract class ShopMapper {
    fun apiEntityToEntity(entity: ShopApiEntity): ShopEntity {
        val shopEntity = ShopEntity()
        shopEntity.shopId = entity.id
        shopEntity.name = entity.name
        shopEntity.description = entity.description
        shopEntity.rating = entity.rating
        shopEntity.address = entity.address
        shopEntity.latitude = entity.latitude
        shopEntity.longitude = entity.longitude
        shopEntity.openingTime = entity.openingTime
        shopEntity.closingTime = entity.closingTime
        shopEntity.imageUrl = entity.imageUrl
        return shopEntity
    }

    fun shopWithServicesToShopModel(entity: ShopWithServices): ShopModel {
        val shopModel = ShopModel()
        shopModel.id = entity.shop!!.shopId
        shopModel.name = entity.shop?.name
        shopModel.description = entity.shop?.description
        shopModel.rating = entity.shop?.rating ?: BigDecimal.ZERO
        shopModel.address = entity.shop?.address
        shopModel.latitude = entity.shop?.latitude
        shopModel.longitude = entity.shop?.longitude
        shopModel.openingTime = entity.shop?.openingTime
        shopModel.closingTime = entity.shop?.closingTime
        shopModel.imageUrl = entity.shop?.imageUrl
        val serviceModelList = mutableListOf<ShopServiceModel>()
        if (entity.serviceList != null) {
            for (service in entity.serviceList!!) {
                val serviceModel = ShopServiceModel()
                serviceModel.id = service.serviceId
                serviceModel.name = service.name
                serviceModelList.add(serviceModel)
            }
        }
        shopModel.serviceList = serviceModelList
        return shopModel
    }

    abstract fun shopWithServicesListToShopModelList(entities: List<ShopWithServices>): List<ShopModel>

}