package cz.fel.barysole.pdaeshop.ui.base

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class BaseItemDecoration(
    private val topSpacing: Int,
    private val bottomSpacing: Int,
    private val leftSpacing: Int,
    private val rightSpacing: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.top = topSpacing
        outRect.left = leftSpacing
        outRect.right = rightSpacing
        outRect.bottom = bottomSpacing
    }

}