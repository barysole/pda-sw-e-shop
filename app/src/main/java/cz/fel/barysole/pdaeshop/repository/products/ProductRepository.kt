package cz.fel.barysole.pdaeshop.repository.products

import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import io.reactivex.rxjava3.core.Single

interface ProductRepository {

    fun loadProducts(): Single<Result>

    fun loadProduct(productId: String): Single<Result>

    class Result(val userError: UserError, val list: List<ProductModel>) {
        val isSuccessful: Boolean
            get() = userError === UserError.NO_ERROR
    }

}