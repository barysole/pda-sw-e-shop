package cz.fel.barysole.pdaeshop.repository.products

import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.ui.products.model.ProductTypeModel
import io.reactivex.rxjava3.core.Single

interface ProductTypeRepository {

    fun loadProductsType(): Single<Result>

    fun changeProductType(productType: ProductTypeModel): Single<Result>

    class Result(val userError: UserError, val list: List<ProductTypeModel>?) {
        val isSuccessful: Boolean
            get() = userError === UserError.NO_ERROR
    }

}