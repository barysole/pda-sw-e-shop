package cz.fel.barysole.pdaeshop.api.model.review

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.pdaeshop.api.model.products.ProductsApiEntity
import java.math.BigDecimal

class ReviewApiEntity {

    @SerializedName("id")
    val id: String = ""

    @SerializedName("author")
    val author: String = ""

    @SerializedName("advantages")
    val advantages: String = ""

    @SerializedName("disadvantages")
    val disadvantages: String = ""

    @SerializedName("rating")
    val rating: BigDecimal = BigDecimal.ZERO

    @SerializedName("creationTimestamp")
    val creationTimestamp: String? = null

    @SerializedName("product")
    val product: ProductsApiEntity = ProductsApiEntity()

}