package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import cz.fel.barysole.pdaeshop.db.room.converter.BigDecimalStringConverter

@Entity(tableName = "ProductType")
@TypeConverters(BigDecimalStringConverter::class)
class ProductTypeEntity {
    @PrimaryKey
    var id: String = ""
    var name: String = ""
    var isInFilter: Boolean = true
}