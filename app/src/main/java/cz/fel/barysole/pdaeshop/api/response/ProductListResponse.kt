package cz.fel.barysole.pdaeshop.api.response

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.pdaeshop.api.model.products.ProductsApiEntity

class ProductListResponse(

    @SerializedName("content")
    val productsList: List<ProductsApiEntity>

)