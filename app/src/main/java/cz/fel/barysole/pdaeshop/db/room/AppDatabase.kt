package cz.fel.barysole.pdaeshop.db.room

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.fel.barysole.pdaeshop.db.room.dao.ProductDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductInCartDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductTypeDao
import cz.fel.barysole.pdaeshop.db.room.dao.ReviewDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopAndServiceDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopServiceDao
import cz.fel.barysole.pdaeshop.db.room.model.ProductEntity
import cz.fel.barysole.pdaeshop.db.room.model.ProductInCartEntity
import cz.fel.barysole.pdaeshop.db.room.model.ProductTypeEntity
import cz.fel.barysole.pdaeshop.db.room.model.ReviewEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopAndServiceEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopEntity
import cz.fel.barysole.pdaeshop.db.room.model.ShopServicesEntity

@Database(
    entities = [ProductEntity::class, ProductTypeEntity::class, ReviewEntity::class, ProductInCartEntity::class, ShopEntity::class, ShopAndServiceEntity::class, ShopServicesEntity::class],
    version = 9,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun productTypeDao(): ProductTypeDao
    abstract fun productInCartDao(): ProductInCartDao
    abstract fun reviewDao(): ReviewDao
    abstract fun shopDao(): ShopDao
    abstract fun shopServiceDao(): ShopServiceDao
    abstract fun shopAndServiceDao(): ShopAndServiceDao
}