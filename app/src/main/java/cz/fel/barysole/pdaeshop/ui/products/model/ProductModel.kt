package cz.fel.barysole.pdaeshop.ui.products.model

import java.math.BigDecimal

class ProductModel {
    var id: String = ""
    var title: String = ""
    var description: String = ""
    var rating: BigDecimal = BigDecimal(0)
    var ratingCount: Int = 0
    var price: BigDecimal = BigDecimal(0)
    var discount: Int = 0
    var imageUrl: String = ""
    var productType: ProductTypeModel? = null
}