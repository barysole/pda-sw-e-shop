package cz.fel.barysole.pdaeshop.ui.personalinformation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentPersonalInformationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PersonalInformationFragment : Fragment() {

    private lateinit var navController: NavController

    private var _binding: FragmentPersonalInformationBinding? = null
    val args: PersonalInformationFragmentArgs by navArgs()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var totalPriceTV: TextView
    private lateinit var personalInfoFirstName: TextInputLayout
    private lateinit var personalInfoSecondName: TextInputLayout
    private lateinit var personalInfoEmail: TextInputLayout
    private lateinit var personalInfoPhoneNumber: TextInputLayout
    private lateinit var personalInfoStreet: TextInputLayout
    private lateinit var personalInfoHouseNumber: TextInputLayout
    private lateinit var personalInfoPostcode: TextInputLayout
    private lateinit var scrollView: NestedScrollView
    private lateinit var continueButton: Button

    private val viewModel by viewModels<PersonalInformationViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPersonalInformationBinding.inflate(inflater, container, false)
        val root: View = binding.root

        totalPriceTV = binding.totalItemPrice
        setTotalPrice(args.totalPrice)

        navController = findNavController()

        scrollView = binding.personalInfoScrollView
        personalInfoFirstName = binding.personalInfoFirstName
        personalInfoSecondName = binding.personalInfoSecondName
        personalInfoEmail = binding.personalInfoEmail
        personalInfoPhoneNumber = binding.personalInfoPhoneNumber
        personalInfoStreet = binding.personalInfoStreet
        personalInfoHouseNumber = binding.personalInfoHouseNumber
        personalInfoPostcode = binding.personalInfoPostcode
        continueButton = binding.continueButton
        continueButton.setOnClickListener {
            validateFields()
        }

        viewModel.apply {
            totalPriceAmountLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { totalPrice -> setTotalPrice(totalPrice) })
            getTotalPrice()
        }

        return root
    }

    private fun setTotalPrice(totalPrice: String) {
        totalPriceTV.text = totalPrice
    }

    private fun validateFields() {
        if (personalInfoFirstName.editText?.text.toString().isNotBlank()
            && personalInfoSecondName.editText?.text.toString().isNotBlank()
            && personalInfoEmail.editText?.text.toString().isNotBlank()
            && personalInfoPhoneNumber.editText?.text.toString().isNotBlank()
            && personalInfoStreet.editText?.text.toString().isNotBlank()
            && personalInfoHouseNumber.editText?.text.toString().isNotBlank()
            && personalInfoPostcode.editText?.text.toString().isNotBlank()
        ) {
            viewModel.deleteProductsInCart()
            val action =
                PersonalInformationFragmentDirections.actionPersonalInformationFragmentToOrderConfirmationFragment()
            navController.navigate(action)
        } else {
            MaterialAlertDialogBuilder(
                requireContext(),
                R.style.ThemeOverlay_App_MaterialAlertDialog
            )
                .setTitle(resources.getString(R.string.validation_failed_title))
                .setMessage(resources.getString(R.string.validation_failed_text))
                .setPositiveButton(resources.getString(R.string.ok_text)) { dialog, which ->
                    scrollView.post { scrollView.fullScroll(View.FOCUS_DOWN) }
                }
                .show()
        }
    }

}