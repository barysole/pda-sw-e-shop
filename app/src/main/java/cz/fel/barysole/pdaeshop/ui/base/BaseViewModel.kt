package cz.fel.barysole.pdaeshop.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    var isStarted: Boolean = false

    fun start() {
        if (!isStarted) {
            isStarted = true
            onStart()
        }
    }

    protected abstract fun onStart()
}