package cz.fel.barysole.pdaeshop.repository.error

class RepositoryException(override val message: String) : Exception(message)