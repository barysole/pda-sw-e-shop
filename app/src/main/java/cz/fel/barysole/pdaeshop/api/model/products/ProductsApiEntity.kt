package cz.fel.barysole.pdaeshop.api.model.products

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class ProductsApiEntity {

    @SerializedName("id")
    val id: String = ""

    @SerializedName("title")
    val title: String = ""

    @SerializedName("description")
    val description: String = ""

    @SerializedName("rating")
    val rating: BigDecimal = BigDecimal(0)

    @SerializedName("ratingCount")
    val ratingCount: Int = 0

    @SerializedName("price")
    val price: BigDecimal = BigDecimal(0)

    @SerializedName("discount")
    val discount: Int = 0

    @SerializedName("imageUrl")
    val imageUrl: String = ""

    @SerializedName("productType")
    val productType: ProductTypeApiEntity? = null

}