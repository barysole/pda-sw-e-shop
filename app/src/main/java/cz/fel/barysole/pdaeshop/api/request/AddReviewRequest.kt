package cz.fel.barysole.pdaeshop.api.request

import com.google.gson.annotations.SerializedName

class AddReviewRequest {
    @SerializedName("author")
    var author: String? = ""

    @SerializedName("advantages")
    var advantages: String? = ""

    @SerializedName("disadvantages")
    var disadvantages: String? = ""

    @SerializedName("rating")
    var rating: String? = ""

    @SerializedName("product")
    var product: Product? = null

    class Product(id: String) {
        @SerializedName("id")
        var id: String? = id
    }

}