package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ShopService")
class ShopServicesEntity {

    @PrimaryKey
    var serviceId: String = ""

    var name: String = ""

}