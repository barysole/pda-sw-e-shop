package cz.fel.barysole.pdaeshop.api.response

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.pdaeshop.api.model.products.ProductsApiEntity

class ProductResponse(

    @SerializedName("content")
    val product: ProductsApiEntity

)