package cz.fel.barysole.pdaeshop.ui.productdetail.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import java.math.BigDecimal
import javax.inject.Inject

class ReviewAdapter @Inject constructor() : BaseRecyclerAdapter<ReviewAdapter.ReviewViewHolder>() {

    private var items = arrayListOf<ReviewModel>()
    var onReviewItemClickListener: ((review: ReviewModel, position: Int) -> Unit)? = null

    inner class ReviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val reviewPositiveDescription: TextView =
            itemView.findViewById(R.id.product_detail_positive_description)
        private val reviewNegativeDescription: TextView =
            itemView.findViewById(R.id.product_detail_negative_description)
        private val reviewDescription: TextView = itemView.findViewById(R.id.review_description)
        private val reviewRating1: ImageView = itemView.findViewById(R.id.review_rating_1)
        private val reviewRating2: ImageView = itemView.findViewById(R.id.review_rating_2)
        private val reviewRating3: ImageView = itemView.findViewById(R.id.review_rating_3)
        private val reviewRating4: ImageView = itemView.findViewById(R.id.review_rating_4)
        private val reviewRating5: ImageView = itemView.findViewById(R.id.review_rating_5)

        init {
            view.setOnClickListener {
                onReviewItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(review: ReviewModel) {
            reviewPositiveDescription.text = review.advantages
            reviewNegativeDescription.text = review.disadvantages
            reviewDescription.text = review.author + " - " + review.creationTimestamp
            var tempRating = review.rating
            val startList = mutableListOf(
                reviewRating1,
                reviewRating2,
                reviewRating3,
                reviewRating4,
                reviewRating5
            )
            while (tempRating > BigDecimal.ZERO) {
                if (tempRating >= BigDecimal.ONE) {
                    startList[0].setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_star_24
                        )
                    )
                    startList.removeAt(0)
                    tempRating = tempRating.minus(BigDecimal.ONE)
                } else if (tempRating > BigDecimal.ZERO) {
                    startList[0].setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_star_half_24
                        )
                    )
                    startList.removeAt(0)
                    tempRating = BigDecimal.ZERO
                }
            }
            for (remainingStar in startList) {
                remainingStar.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.baseline_star_border_24
                    )
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_review, parent, false)
        return ReviewViewHolder(view)
    }


    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ReviewModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}
