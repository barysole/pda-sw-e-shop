package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

class ShopWithServices {

    @Embedded
    var shop: ShopEntity? = null

    @Relation(
        parentColumn = "shopId",
        entityColumn = "serviceId",
        associateBy = Junction(ShopAndServiceEntity::class)
    )
    var serviceList: List<ShopServicesEntity>? = null

}