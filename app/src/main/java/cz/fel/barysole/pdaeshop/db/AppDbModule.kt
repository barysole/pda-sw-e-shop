package cz.fel.barysole.pdaeshop.db

import android.content.Context
import androidx.room.Room
import cz.fel.barysole.pdaeshop.db.room.AppDatabase
import cz.fel.barysole.pdaeshop.db.room.dao.ProductDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductInCartDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductTypeDao
import cz.fel.barysole.pdaeshop.db.room.dao.ReviewDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopAndServiceDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopDao
import cz.fel.barysole.pdaeshop.db.room.dao.ShopServiceDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppDbModule {

    @Singleton
    @Provides
    fun provideAppDb(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideProductDao(appDatabase: AppDatabase): ProductDao {
        return appDatabase.productDao()
    }

    @Singleton
    @Provides
    fun provideProductTypeDao(appDatabase: AppDatabase): ProductTypeDao {
        return appDatabase.productTypeDao()
    }

    @Singleton
    @Provides
    fun provideReviewDao(appDatabase: AppDatabase): ReviewDao {
        return appDatabase.reviewDao()
    }

    @Singleton
    @Provides
    fun provideProductInCartDao(appDatabase: AppDatabase): ProductInCartDao {
        return appDatabase.productInCartDao()
    }

    @Singleton
    @Provides
    fun provideShopDao(appDatabase: AppDatabase): ShopDao {
        return appDatabase.shopDao()
    }

    @Singleton
    @Provides
    fun provideShopServiceDao(appDatabase: AppDatabase): ShopServiceDao {
        return appDatabase.shopServiceDao()
    }

    @Singleton
    @Provides
    fun provideShopAndServiceDao(appDatabase: AppDatabase): ShopAndServiceDao {
        return appDatabase.shopAndServiceDao()
    }

    companion object {
        const val DB_NAME = "cz.barysole.app.db"
    }

}