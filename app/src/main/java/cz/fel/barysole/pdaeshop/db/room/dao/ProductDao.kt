package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ProductEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface ProductDao : BaseDao<ProductEntity> {

    @Query("SELECT * FROM Product WHERE id = :id")
    fun getById(id: String): ProductEntity?

    @Query("SELECT * FROM Product WHERE id = :id")
    fun getByIdRx(id: String): Flowable<ProductEntity>

    @Query("SELECT * FROM Product")
    fun getAll(): List<ProductEntity>

    @Query("SELECT * FROM Product")
    fun getAllRx(): Flowable<List<ProductEntity>>

    @Query("DELETE FROM Product")
    fun deleteAll()
}