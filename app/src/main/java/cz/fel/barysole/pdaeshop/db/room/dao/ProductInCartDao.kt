package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ProductInCartEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface ProductInCartDao : BaseDao<ProductInCartEntity> {

    @Query("SELECT * FROM ProductInCart WHERE productId = :id")
    fun getById(id: String): ProductInCartEntity?

    @Query("SELECT * FROM ProductInCart WHERE productId = :id")
    fun getByIdRx(id: String): Flowable<ProductInCartEntity>

    @Query("SELECT * FROM ProductInCart")
    fun getAll(): List<ProductInCartEntity>

    @Query("SELECT * FROM ProductInCart")
    fun getAllRx(): Flowable<List<ProductInCartEntity>>

    @Query("DELETE FROM ProductInCart")
    fun deleteAll()
}