package cz.fel.barysole.pdaeshop.repository.products

import cz.fel.barysole.pdaeshop.api.EshopServerApi
import cz.fel.barysole.pdaeshop.api.model.products.ProductTypeApiEntity
import cz.fel.barysole.pdaeshop.db.room.dao.ProductDao
import cz.fel.barysole.pdaeshop.db.room.dao.ProductTypeDao
import cz.fel.barysole.pdaeshop.repository.error.RepositoryException
import cz.fel.barysole.pdaeshop.repository.error.SimpleUserError
import cz.fel.barysole.pdaeshop.repository.error.UserError
import cz.fel.barysole.pdaeshop.repository.mapper.ProductMapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepositoryImpl @Inject constructor(
    private val productDao: ProductDao,
    private val productTypeDao: ProductTypeDao,
    private val productsApi: EshopServerApi,
    private val productMapper: ProductMapper
) :
    ProductRepository {
    override fun loadProducts(): Single<ProductRepository.Result> {
        return productsApi.getAllProducts()
            .doOnSuccess { response ->
                if (response.isSuccessful) {
                    val productList = response.body()!!.productsList
                    productDao.deleteAll()
                    for (product in productList) {
                        productDao.insert(productMapper.apiEntityToEntity(product))
                        productTypeDao.insert(
                            productMapper.productTypeApiEntityToEntity(
                                product.productType ?: ProductTypeApiEntity()
                            )
                        )
                    }
                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .flatMap { Single.fromCallable { productDao.getAll() } }
            .map { productEntityList ->
                val productList = productMapper.entityListToModel(productEntityList)
                ProductRepository.Result(UserError.NO_ERROR, productList)
            }
            .onErrorReturn { error ->
                try {
                    ProductRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), productMapper.entityListToModel(productDao.getAll())
                    )
                } catch (e: Exception) {
                    ProductRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), emptyList()
                    )
                }
            }
    }

    //the result list contains only one item if success
    override fun loadProduct(productId: String): Single<ProductRepository.Result> {
        return productsApi.getProductWithId(productId)
            .doOnSuccess { response ->
                if (response.isSuccessful) {
                    val product = response.body()!!
                    if (productDao.getById(product.id) != null) {
                        productDao.update(productMapper.apiEntityToEntity(product))
                    } else {
                        productDao.insert(productMapper.apiEntityToEntity(product))
                    }
                } else {
                    throw RepositoryException("Error with code: " + response.code().toString())
                }
            }
            .flatMap { Single.fromCallable { productDao.getById(productId)!! } }
            .map { productEntity ->
                val productModel = productMapper.entityToModel(productEntity)
                ProductRepository.Result(UserError.NO_ERROR, listOf(productModel))
            }
            .onErrorReturn { error ->
                try {
                    ProductRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), listOf(productMapper.entityToModel(productDao.getById(productId)!!))
                    )
                } catch (e: Exception) {
                    ProductRepository.Result(
                        SimpleUserError(
                            error.message ?: "Something went wrong..."
                        ), emptyList()
                    )
                }
            }
    }
}