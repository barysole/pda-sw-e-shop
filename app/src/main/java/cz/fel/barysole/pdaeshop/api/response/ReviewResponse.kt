package cz.fel.barysole.pdaeshop.api.response

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.pdaeshop.api.model.review.ReviewApiEntity

class ReviewResponse(

    @SerializedName("content")
    val reviewList: List<ReviewApiEntity>

)