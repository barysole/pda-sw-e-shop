package cz.fel.barysole.pdaeshop.repository.mapper

import cz.fel.barysole.pdaeshop.api.model.productInCart.ProductInCartApiEntity
import cz.fel.barysole.pdaeshop.db.room.model.ProductInCartEntity
import cz.fel.barysole.pdaeshop.repository.mapper.base.BaseMapper
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import org.mapstruct.Mapper

@Mapper
interface ProductInCartMapper :
    BaseMapper<ProductInCartModel, ProductInCartApiEntity, ProductInCartEntity> {
}