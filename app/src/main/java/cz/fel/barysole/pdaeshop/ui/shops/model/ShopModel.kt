package cz.fel.barysole.pdaeshop.ui.shops.model

import java.math.BigDecimal

class ShopModel {

    var id: String = ""

    var name: String? = null

    var description: String? = null

    var rating: BigDecimal = BigDecimal(0)

    var address: String? = null

    var latitude: BigDecimal? = null

    var longitude: BigDecimal? = null

    var openingTime: String? = null

    var closingTime: String? = null

    var imageUrl: String? = null

    var serviceList: List<ShopServiceModel>? = null
}