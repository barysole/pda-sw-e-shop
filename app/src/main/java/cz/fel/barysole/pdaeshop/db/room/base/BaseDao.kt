package cz.fel.barysole.pdaeshop.db.room.base

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<Entity> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entity: Entity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entities: List<Entity>): LongArray

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun update(entity: Entity)

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun update(entities: List<Entity>)

    @Delete
    fun delete(entity: Entity)

    @Delete
    fun delete(entities: List<Entity>)
}