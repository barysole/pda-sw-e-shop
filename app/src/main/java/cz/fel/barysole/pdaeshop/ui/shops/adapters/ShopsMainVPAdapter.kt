package cz.fel.barysole.pdaeshop.ui.shops.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import cz.fel.barysole.pdaeshop.ui.shoplist.ShopListFragment
import cz.fel.barysole.pdaeshop.ui.shopmap.ShopMapFragment

class ShopsMainVPAdapter(viewPagerFragment: Fragment) : FragmentStateAdapter(viewPagerFragment) {

    override fun getItemCount(): Int {
        return 2;
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ShopListFragment()
            1 -> ShopMapFragment()
            else -> {
                throw RuntimeException("Undefined fragment on position $position")
            }
        }
    }

}