package cz.fel.barysole.pdaeshop.api.model.products

import com.google.gson.annotations.SerializedName

class ProductTypeApiEntity {

    @SerializedName("id")
    val id: String = ""

    @SerializedName("name")
    val name: String = ""

}