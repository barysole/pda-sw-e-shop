package cz.fel.barysole.pdaeshop.ui.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentProductsBinding
import cz.fel.barysole.pdaeshop.ui.base.AutoFitGridLayoutManager
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.products.adapter.GridItemDecoration
import cz.fel.barysole.pdaeshop.ui.products.adapter.ProductsAdapter
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProductsFragment : BaseFragment() {

    private var _binding: FragmentProductsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView
    private lateinit var filterFAB: FloatingActionButton

    @Inject
    lateinit var adapter: ProductsAdapter

    private val productsViewModel by viewModels<ProductsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        loaderHolder = binding.loaderHolder
        loaderIcon = binding.loaderIcon
        noDataButton = binding.noDataButton
        noDataTextView = binding.noDataText
        filterFAB = binding.filterFab

        noDataButton?.setOnClickListener {
            productsViewModel.loadProductsWithFilters(true)
        }
        val navController = findNavController()
        filterFAB.setOnClickListener {
            val action =
                ProductsFragmentDirections
                    .actionProductsFragmentToProductFilterFragment()
            navController.navigate(action)
        }

        recyclerView = binding.recyclerProductsList
        recyclerView.addItemDecoration(GridItemDecoration(resources.getDimensionPixelOffset(R.dimen.product_list_item_offset)))

        val layoutManager = AutoFitGridLayoutManager(requireContext(), 400);
        recyclerView.layoutManager = layoutManager;
        recyclerView.adapter =
            adapter.apply {
                onProductItemClickListener = { product, position ->
                    val action =
                        ProductsFragmentDirections.actionProductsFragmentToProductDetailFragment(
                            product.id
                        )
                    navController.navigate(action)
                }
                onProductAddToCartClickListener = { product, position ->
                    productsViewModel.onProductAddToCart(product)
                }
            }

        productsViewModel.apply {
            productsLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { products -> drawProductsData(products) })
            isLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            noDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> showError(noData) })
            productAddedSuccessfullyLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isAddedSuccess -> if (isAddedSuccess) showInfo("Product has been successfully added.") })
            start()
            loadProductsWithFilters(false)
        }

        return root
    }

    private fun drawProductsData(items: List<ProductModel>) {
        adapter.setItems(ArrayList(items))
    }

    override fun setNoData(noData: Boolean) {
        super.setNoData(noData)
        if (noData) {
            filterFAB.visibility = View.GONE
            recyclerView.visibility = View.INVISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            filterFAB.visibility = View.VISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}