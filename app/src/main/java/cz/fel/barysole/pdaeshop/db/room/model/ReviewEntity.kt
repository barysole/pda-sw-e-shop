package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import cz.fel.barysole.pdaeshop.db.room.converter.BigDecimalStringConverter
import java.math.BigDecimal

@Entity(tableName = "Review")
@TypeConverters(BigDecimalStringConverter::class)
class ReviewEntity {

    @PrimaryKey
    var id: String = ""

    var author: String = ""

    var advantages: String = ""

    var disadvantages: String = ""

    var rating: BigDecimal = BigDecimal.ZERO

    var creationTimestamp: String? = null

    var productId: String = ""

}