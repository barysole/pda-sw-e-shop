package cz.fel.barysole.pdaeshop.db.room.dao

import androidx.room.Dao
import androidx.room.Query
import cz.fel.barysole.pdaeshop.db.room.base.BaseDao
import cz.fel.barysole.pdaeshop.db.room.model.ReviewEntity
import io.reactivex.rxjava3.core.Flowable

@Dao
interface ReviewDao : BaseDao<ReviewEntity> {

    @Query("SELECT * FROM Review WHERE id = :id")
    fun getById(id: String): ReviewEntity

    @Query("SELECT * FROM Review WHERE id = :id")
    fun getByIdRx(id: String): Flowable<ReviewEntity>

    @Query("SELECT * FROM Review")
    fun getAll(): List<ReviewEntity>

    @Query("SELECT * FROM Review")
    fun getAllRx(): Flowable<List<ReviewEntity>>

    @Query("DELETE FROM Review")
    fun deleteAll()

    @Query("SELECT * FROM Review WHERE productId = :productId")
    fun getByProductId(productId: String): List<ReviewEntity>
}