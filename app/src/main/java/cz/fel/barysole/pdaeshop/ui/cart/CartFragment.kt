package cz.fel.barysole.pdaeshop.ui.cart

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.divider.MaterialDivider
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.databinding.FragmentCartBinding
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.base.BaseItemDecoration
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.cart.adapter.ProductsInCartAdapter
import cz.fel.barysole.pdaeshop.ui.cart.model.ProductInCartModel
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : BaseFragment() {

    private var _binding: FragmentCartBinding? = null

    private lateinit var recyclerView: RecyclerView

    private lateinit var bottomSheetBg: View
    private lateinit var bottomSheetContainer: ViewGroup
    private lateinit var chosenItemImage: ImageView
    private lateinit var chosenItemTitle: TextView
    private lateinit var chosenItemPrice: TextView
    private lateinit var chosenItemMinusAmount: Button
    private lateinit var chosenItemPlusAmount: Button
    private lateinit var chosenItemAmount: TextView
    private lateinit var saveChangesButton: Button
    private lateinit var totalPriceTV: TextView
    private lateinit var continueButton: Button
    private lateinit var toPayTV: TextView
    private lateinit var totalPriceDivider: MaterialDivider

    private lateinit var navController: NavController

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @Inject
    lateinit var adapter: ProductsInCartAdapter
    private val cartViewModel by viewModels<CartViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        val root: View = binding.root

        navController = findNavController()
        bottomSheetBg = binding.bottomSheetBg
        bottomSheetContainer = binding.bottomSheetContainer
        chosenItemImage = binding.chosenItemImage
        chosenItemTitle = binding.chosenItemTitle
        chosenItemPrice = binding.chosenItemPrice
        chosenItemMinusAmount = binding.chosenItemMinusAmount
        chosenItemPlusAmount = binding.chosenItemPlusAmount
        chosenItemAmount = binding.chosenItemAmount
        saveChangesButton = binding.saveChangesButton
        noDataTextView = binding.noDataText
        totalPriceTV = binding.totalItemPrice
        toPayTV = binding.totalItemPriceTitle
        totalPriceDivider = binding.totalPriceDivider
        recyclerView = binding.recyclerProductsInTheCartList
        recyclerView.adapter = adapter.apply {
            onProductInCartItemClickListener = { product, position ->
                cartViewModel.onChoseProduct(product)
            }
            onProductItemDeleteClickListener = { product, position ->
                showConfirmationDialog(product)
            }
        }
        continueButton = binding.continueButton
        saveChangesButton.setOnClickListener {
            cartViewModel.onChangeProductAmount(
                chosenItemAmount.text.toString().toInt()
            )
        }
        chosenItemMinusAmount.setOnClickListener { decreaseAmount() }
        chosenItemPlusAmount.setOnClickListener { increaseAmount() }
        bottomSheetBg.setOnClickListener { cartViewModel.onChoseProduct(null) }
        recyclerView.addItemDecoration(
            BaseItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.product_in_the_cart_margin),
                resources.getDimensionPixelOffset(R.dimen.zero_dp),
                resources.getDimensionPixelOffset(R.dimen.zero_dp)
            )
        )

        cartViewModel.apply {
            productsInCartLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { productsInTheCart -> showProductsInTheCart(productsInTheCart) })
            noDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { error -> showError(error) })
            chosenProductLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { product ->
                    if (product != null) {
                        changeVisibilityOfBottomSheet(true)
                        fillBottomSheet(product)
                    } else {
                        changeVisibilityOfBottomSheet(false)
                    }
                })
            loadProductsInCart()
        }

        return root
    }

    private fun changeVisibilityOfBottomSheet(isShowing: Boolean) {
        if (isShowing) {
            bottomSheetBg.visibility = View.VISIBLE
            bottomSheetContainer.visibility = View.VISIBLE
        } else {
            bottomSheetBg.visibility = View.GONE
            bottomSheetContainer.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun fillBottomSheet(productInCart: ProductInCartModel) {
        ImageLoader(
            chosenItemImage,
            R.drawable.e_shop_logo_icon_with_padding,
            productInCart.product.imageUrl
        ).load()
        chosenItemTitle.text = productInCart.product.title
        chosenItemPrice.text =
            (productInCart.product.price.multiply(BigDecimal(productInCart.amount))).toString() + " Kč"
        chosenItemAmount.text = productInCart.amount.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun increaseAmount() {
        val previousAmount = chosenItemAmount.text.toString().toInt()
        val previousPrice = chosenItemPrice.text.toString().dropLast(3).toBigDecimal()
        chosenItemPrice.text = previousPrice.plus(
            previousPrice.divide(
                previousAmount.toBigDecimal(),
                RoundingMode.HALF_UP
            )
        ).toString() + " Kč"
        chosenItemAmount.text = (previousAmount + 1).toString()
    }

    @SuppressLint("SetTextI18n")
    private fun decreaseAmount() {
        val previousAmount = chosenItemAmount.text.toString().toInt()
        if (previousAmount != 1) {
            val previousPrice = chosenItemPrice.text.toString().dropLast(3).toBigDecimal()
            chosenItemPrice.text = previousPrice.minus(
                previousPrice.divide(
                    previousAmount.toBigDecimal(),
                    RoundingMode.HALF_UP
                )
            ).toString() + " Kč"
            chosenItemAmount.text = (previousAmount - 1).toString()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showProductsInTheCart(productsInTheCart: List<ProductInCartModel>) {
        if (productsInTheCart.isEmpty()) {
            continueButton.visibility = View.GONE
            toPayTV.visibility = View.GONE
            totalPriceDivider.visibility = View.INVISIBLE
            adapter.setItems(ArrayList())
        } else {
            totalPriceDivider.visibility = View.VISIBLE
            toPayTV.visibility = View.VISIBLE
            continueButton.visibility = View.VISIBLE
            var totalPrice = BigDecimal.ZERO;
            for (product in productsInTheCart) {
                totalPrice =
                    totalPrice.plus(product.product.price.multiply(product.amount.toBigDecimal()))
            }
            adapter.setItems(ArrayList(productsInTheCart))
            totalPriceTV.text = "$totalPrice Kč"
            continueButton.setOnClickListener {
                val action =
                    CartFragmentDirections.actionCartFragmentToPersonalInformationFragment(
                        "$totalPrice Kč"
                    )
                navController.navigate(action)
            }
        }
    }

    private fun showConfirmationDialog(product: ProductInCartModel) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.delete_confirmation_dialog_header))
            .setMessage(resources.getString(R.string.delete_confirmation_dialog_text))
            .setNegativeButton(resources.getString(R.string.delete_confirmation_dialog_reject)) { dialog, which ->
                // Respond to negative button press
            }
            .setPositiveButton(resources.getString(R.string.delete_confirmation_dialog_confirm)) { dialog, which ->
                cartViewModel.onChangeProductAmount(0, product)
            }
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}