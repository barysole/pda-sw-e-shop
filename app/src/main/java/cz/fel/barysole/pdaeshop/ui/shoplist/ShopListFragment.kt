package cz.fel.barysole.pdaeshop.ui.shoplist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseFragment
import cz.fel.barysole.pdaeshop.ui.shoplist.adapter.ShopListAdapter
import cz.fel.barysole.pdaeshop.ui.shops.ShopsFragmentDirections
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShopListFragment : BaseFragment() {

    private lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var adapter: ShopListAdapter

    private val shopListViewModel by activityViewModels<ShopListViewModel>()
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_shop_list, container, false)

        loaderHolder = view.findViewById(R.id.loaderHolder)
        loaderIcon = view.findViewById(R.id.loaderIcon)
        noDataButton = view.findViewById(R.id.noDataButton)
        noDataTextView = view.findViewById(R.id.noDataText)

        noDataButton?.setOnClickListener {
            shopListViewModel.loadShopList()
        }
        navController = findNavController()
        recyclerView = view.findViewById(R.id.recycler_shop_list)
        recyclerView.adapter = adapter.apply {
            onShopItemClickListener = { shop, position ->
                val action =
                    ShopsFragmentDirections.actionShopsFragmentToShopDetailFragment(
                        shop.id
                    )
                navController.navigate(action)
            }
        }
        shopListViewModel.apply {
            shopListLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { shopList -> drawShopList(shopList) })
            isLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            noDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> setNoData(noData) })
            errorLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { noData -> showError(noData) })
            start()
        }

        return view
    }

    private fun drawShopList(shopList: List<ShopModel>) {
        adapter.setItems(ArrayList(shopList))
    }

}