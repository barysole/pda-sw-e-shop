package cz.fel.barysole.pdaeshop.ui.products.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel
import java.math.BigDecimal
import javax.inject.Inject

class ProductsAdapter @Inject constructor() :
    BaseRecyclerAdapter<ProductsAdapter.ProductViewHolder>() {

    private var items = arrayListOf<ProductModel>()
    var onProductItemClickListener: ((product: ProductModel, position: Int) -> Unit)? = null
    var onProductAddToCartClickListener: ((product: ProductModel, position: Int) -> Unit)? = null

    inner class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val productImage: ImageView = itemView.findViewById(R.id.product_image)
        private val productTitle: TextView = itemView.findViewById(R.id.product_title)
        private val productRating1: ImageView = itemView.findViewById(R.id.product_rating_1)
        private val productRating2: ImageView = itemView.findViewById(R.id.product_rating_2)
        private val productRating3: ImageView = itemView.findViewById(R.id.product_rating_3)
        private val productRating4: ImageView = itemView.findViewById(R.id.product_rating_4)
        private val productRating5: ImageView = itemView.findViewById(R.id.product_rating_5)
        private val productPrice: TextView = itemView.findViewById(R.id.product_price)
        private val addToCart: Button = itemView.findViewById(R.id.add_to_cart_button)

        init {
            view.setOnClickListener {
                onProductItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(product: ProductModel, position: Int) {
            ImageLoader(
                productImage,
                R.drawable.e_shop_logo_icon_with_padding,
                product.imageUrl
            ).load()
            productTitle.text = product.title
            productPrice.text = product.price.toString() + " Kč"
            addToCart.setOnClickListener {
                onProductAddToCartClickListener?.let { it1 ->
                    it1(
                        product,
                        position
                    )
                }
            }
            var tempRating = product.rating
            val startList = mutableListOf(
                productRating1,
                productRating2,
                productRating3,
                productRating4,
                productRating5
            )
            while (tempRating > BigDecimal.ZERO) {
                if (tempRating >= BigDecimal.ONE) {
                    startList[0].setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_star_24
                        )
                    )
                    startList.removeAt(0)
                    tempRating = tempRating.minus(BigDecimal.ONE)
                } else if (tempRating > BigDecimal.ZERO) {
                    startList[0].setImageDrawable(
                        AppCompatResources.getDrawable(
                            context,
                            R.drawable.baseline_star_half_24
                        )
                    )
                    startList.removeAt(0)
                    tempRating = BigDecimal.ZERO
                }
            }
            for (remainingStar in startList) {
                remainingStar.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.baseline_star_border_24
                    )
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_list, parent, false)
        return ProductViewHolder(view)
    }


    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ProductModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}
