package cz.fel.barysole.pdaeshop.ui.cart.model

import cz.fel.barysole.pdaeshop.ui.products.model.ProductModel

class ProductInCartModel {
    var product: ProductModel = ProductModel()
    var amount: Int = 0
}