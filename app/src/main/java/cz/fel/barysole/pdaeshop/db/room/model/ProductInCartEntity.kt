package cz.fel.barysole.pdaeshop.db.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import cz.fel.barysole.pdaeshop.db.room.converter.BigDecimalStringConverter

@Entity(tableName = "ProductInCart")
@TypeConverters(BigDecimalStringConverter::class)
class ProductInCartEntity {
    @PrimaryKey
    var productId: String = ""
    var amount: Int = 0
}