package cz.fel.barysole.pdaeshop.repository.mapper

import cz.fel.barysole.pdaeshop.api.model.review.ReviewApiEntity
import cz.fel.barysole.pdaeshop.db.room.model.ReviewEntity
import cz.fel.barysole.pdaeshop.ui.products.model.ReviewModel
import org.mapstruct.Mapper
import java.text.SimpleDateFormat

@Mapper
abstract class ReviewMapper {
    fun apiEntityToEntity(entity: ReviewApiEntity): ReviewEntity {
        val reviewEntity = ReviewEntity()
        reviewEntity.id = entity.id
        reviewEntity.author = entity.author
        reviewEntity.advantages = entity.advantages
        reviewEntity.disadvantages = entity.disadvantages
        reviewEntity.rating = entity.rating
        val tempDate =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS").parse(entity.creationTimestamp)
        reviewEntity.creationTimestamp = SimpleDateFormat("yyyy.MM.dd").format(tempDate)
        reviewEntity.productId = entity.product.id
        return reviewEntity
    }

    abstract fun apiEntityListToEntityList(entities: List<ReviewApiEntity>): List<ReviewEntity>

    abstract fun entityListToModelList(entities: List<ReviewEntity>): List<ReviewModel>
}