package cz.fel.barysole.pdaeshop.repository

import cz.fel.barysole.pdaeshop.repository.mapper.ProductMapper
import cz.fel.barysole.pdaeshop.repository.mapper.ReviewMapper
import cz.fel.barysole.pdaeshop.repository.mapper.ShopMapper
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepository
import cz.fel.barysole.pdaeshop.repository.productInCart.CartRepositoryImpl
import cz.fel.barysole.pdaeshop.repository.products.ProductRepository
import cz.fel.barysole.pdaeshop.repository.products.ProductRepositoryImpl
import cz.fel.barysole.pdaeshop.repository.products.ProductTypeRepository
import cz.fel.barysole.pdaeshop.repository.products.ProductTypeRepositoryImpl
import cz.fel.barysole.pdaeshop.repository.review.ReviewRepository
import cz.fel.barysole.pdaeshop.repository.review.ReviewRepositoryImpl
import cz.fel.barysole.pdaeshop.repository.shopandservice.ShopAndServiceRepository
import cz.fel.barysole.pdaeshop.repository.shopandservice.ShopAndServiceRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.mapstruct.factory.Mappers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    internal fun productMapper(): ProductMapper {
        return Mappers.getMapper(ProductMapper::class.java)
    }

    @Provides
    @Singleton
    internal fun reviewMapper(): ReviewMapper {
        return Mappers.getMapper(ReviewMapper::class.java)
    }

    @Provides
    @Singleton
    internal fun shopMapper(): ShopMapper {
        return Mappers.getMapper(ShopMapper::class.java)
    }

    @Singleton
    @Provides
    fun provideProductRepository(productRepositoryImpl: ProductRepositoryImpl): ProductRepository {
        return productRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideProductTypeRepository(productTypeRepositoryImpl: ProductTypeRepositoryImpl): ProductTypeRepository {
        return productTypeRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideReviewRepository(reviewRepositoryImpl: ReviewRepositoryImpl): ReviewRepository {
        return reviewRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideCartRepository(cartRepositoryImpl: CartRepositoryImpl): CartRepository {
        return cartRepositoryImpl
    }

    @Singleton
    @Provides
    fun provideShopAndServiceRepository(shopAndServiceRepositoryImpl: ShopAndServiceRepositoryImpl): ShopAndServiceRepository {
        return shopAndServiceRepositoryImpl
    }

}