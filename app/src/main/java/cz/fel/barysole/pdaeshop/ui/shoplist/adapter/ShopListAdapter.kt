package cz.fel.barysole.pdaeshop.ui.shoplist.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.BaseRecyclerAdapter
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import javax.inject.Inject

class ShopListAdapter @Inject constructor() :
    BaseRecyclerAdapter<ShopListAdapter.ShopViewHolder>() {

    private var items = arrayListOf<ShopModel>()
    var onShopItemClickListener: ((shop: ShopModel, position: Int) -> Unit)? =
        null

    inner class ShopViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val shopImage: ImageView = itemView.findViewById(R.id.shop_image)
        private val shopTitle: TextView = itemView.findViewById(R.id.shop_title)
        private val shopDescription: TextView = itemView.findViewById(R.id.shop_description)

        init {
            view.setOnClickListener {
                onShopItemClickListener?.invoke(items[adapterPosition], adapterPosition)
            }
        }

        fun bind(shopModel: ShopModel, position: Int) {
            ImageLoader(
                shopImage,
                R.drawable.e_shop_logo_icon_with_padding,
                shopModel.imageUrl,
                isCenterCrop = true
            ).load()
            shopTitle.text = shopModel.name
            shopDescription.text = shopModel.address
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_shop, parent, false)
        return ShopViewHolder(view)
    }


    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: ArrayList<ShopModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}