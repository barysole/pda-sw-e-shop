package cz.fel.barysole.pdaeshop.api.model.shop

import com.google.gson.annotations.SerializedName

class ShopServiceApiEntity {

    @SerializedName("id")
    val id: String = ""

    @SerializedName("name")
    val name: String? = null

}