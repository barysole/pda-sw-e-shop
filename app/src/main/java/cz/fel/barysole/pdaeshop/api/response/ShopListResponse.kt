package cz.fel.barysole.pdaeshop.api.response

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.pdaeshop.api.model.shop.ShopApiEntity

class ShopListResponse(

    @SerializedName("content")
    val shopList: List<ShopApiEntity>

)