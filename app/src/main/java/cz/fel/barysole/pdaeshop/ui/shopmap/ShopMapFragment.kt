package cz.fel.barysole.pdaeshop.ui.shopmap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import cz.fel.barysole.pdaeshop.R
import cz.fel.barysole.pdaeshop.ui.base.ImageLoader
import cz.fel.barysole.pdaeshop.ui.shoplist.ShopListViewModel
import cz.fel.barysole.pdaeshop.ui.shops.ShopsFragmentDirections
import cz.fel.barysole.pdaeshop.ui.shops.model.ShopModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ShopMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private val shopListViewModel by activityViewModels<ShopListViewModel>()
    private lateinit var googleMap: GoogleMap
    private val prague: LatLng = LatLng(50.0869556, 14.4068150)
    private lateinit var bottomSheetContainer: ViewGroup
    private lateinit var seeDetailBtn: Button
    private lateinit var chosenShopImage: ImageView
    private lateinit var chosenShopTitle: TextView
    private lateinit var chosenShopDescription: TextView

    private lateinit var navController: NavController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_shop_map, container, false)

        bottomSheetContainer = view.findViewById(R.id.bottom_sheet_container)
        bottomSheetContainer.setOnClickListener { shopListViewModel.onChooseShop(null) }
        chosenShopImage = view.findViewById(R.id.shop_image)
        chosenShopTitle = view.findViewById(R.id.shop_title)
        chosenShopDescription = view.findViewById(R.id.shop_description)
        seeDetailBtn = view.findViewById(R.id.deail_button)

        navController = findNavController()

        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        shopListViewModel.apply {
            shopListLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { shopList -> drawShopList(shopList) })
            chosenShopLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { shopList -> fillBottomSheet(shopList) })
            start()
        }
        val cameraPosition = CameraPosition.Builder()
            .target(prague) // Camera center
            .zoom(12f) // Camera zoom
            .build()
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        googleMap.setOnMarkerClickListener(this);
    }

    private fun drawShopList(shopList: List<ShopModel>) {
        for (shop in shopList) {
            if (shop.longitude != null && shop.latitude != null) {
                val marker = googleMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(shop.latitude!!.toDouble(), shop.longitude!!.toDouble()))
                        .title(shop.name)
                )
                marker?.tag = shop
            }
        }
    }

    private fun fillBottomSheet(shopModel: ShopModel?) {
        if (shopModel != null) {
            seeDetailBtn.setOnClickListener {
                val action =
                    ShopsFragmentDirections.actionShopsFragmentToShopDetailFragment(
                        shopModel.id
                    )
                navController.navigate(action)
            }
            bottomSheetContainer.visibility = View.VISIBLE
            ImageLoader(
                chosenShopImage,
                R.drawable.e_shop_logo_icon_with_padding,
                shopModel.imageUrl,
                isCenterCrop = true
            ).load()
            chosenShopTitle.text = shopModel.name
            chosenShopDescription.text = shopModel.address
        } else {
            bottomSheetContainer.visibility = View.GONE
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val chosenShop = marker.tag as ShopModel

        shopListViewModel.onChooseShop(chosenShop)

        return false
    }


}