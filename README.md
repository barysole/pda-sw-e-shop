## MAIN GOAL AND APPLICATION DESCRIPTION
Let's imagine that some retail chain "E-shop" exists. The retail chain owns several shops that spread
around some area. Through those shops " E-shop" sells their products to clients, but the retail chain
realizes the need of creating mobile application that will allow clients to shop online. The main goal of
the semester project is to describe (d1) and develop (d2) such mobile applications that will implement
following functionality: provide to user information about available "E-shop" shops and their reviews,
allows users to look at the product list and form the shopping basket, allows users to provide their
billing information and make orders, etc. The mobile application will use the server by
sending/receiving HTTP requests to interact with the data.

## APPLICATION APK
https://gitlab.fel.cvut.cz/barysole/pda-sw-e-shop/-/blob/master/apk_build/eshop_app.apk

## SHORT SERVER DESCRIPTION
Custom server was written especially for the purpose of this semestral work. The server provides REST
endpoints for products, reviews and shops. The server has their own REST API documentation defined
using Open API standard (Swagger). For simplicity and server cross platforming the server application
was written in Java programming language and based on the Spring framework. All data the server
contains was scrambled from public internet resources. Source code is available on FEL Gitlab -
https://gitlab.fel.cvut.cz/barysole/eshop-retail-chain-java-backend. 1

## APPLICATION DEPENDENCIES
The mobile application uses a lot of widely-used dependencies to implement some of its different
complex and common parts. Detailed description of those dependencies is large and because of that
here is only a short list with all dependencies in use.
Dependency list:

• Android workers - is the recommended solution for persistent work. Work is persistent when
it remains scheduled through app restarts and system reboots. Because most background
processing is best accomplished through persistent work, WorkManager is the primary
recommended API for background processing.
(https://developer.android.com/topic/libraries/architecture/workmanager)

• Google maps – maps sdk for using Google Maps.
(https://developers.google.com/maps/documentation/android-sdk/overview)

• Glide - is a fast and efficient open source media management and image loading framework
for Android that wraps media decoding, memory and disk caching, and resource pooling into
a simple and easy to use interface. (https://github.com/bumptech/glide)

• Mapstruct - is a code generator that simplifies the implementation of mappings between
Java bean types based on a convention over configuration approach.
(https://mapstruct.org/)

• Rx java 3 – is API for asynchronous programming with observable streams.
(https://reactivex.io/)

• Retrofit – is a type-safe HTTP client for Android and Java.
(https://github.com/square/retrofit)

• Room – is a Database Object Mapping library that makes it easy to access database on
Android applications. (https://developer.android.com/reference/androidx/room/packagesummary)

• Navigation component – is library that implements navigation, from simple button clicks to
more complex patterns, such as app bars and the navigation drawer. The Navigation
component also ensures a consistent and predictable user experience.
(https://developer.android.com/guide/navigation)

• Material Design 3 - is an adaptable system of guidelines, components, and tools that support
the best practices of user interface design. (https://m3.material.io/)
